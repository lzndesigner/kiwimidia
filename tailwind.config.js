module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    zIndex: {
      '0': 0,
      '1': 1,
      '2': 2,
      '3': 3,
      '4': 4,
      '5': 5,
      '10': 10,
      '11': 11,
      '12': 12,
      '13': 13,
      '14': 14,
      '15': 15,
      '16': 16,
      '17': 17,
      '18': 18,
      '19': 19,
      '20': 20,
      '50': 50,
    },
    extend: {
      animation: {
        logo: 'logo 6s ease',
        logoButton: 'logoButton 6.3s ease',
        clouds: 'clouds 4s ease',
        mountain1: 'mountain1 4s ease',
        mountain2: 'mountain2 4s ease',
        mountain3: 'mountain3 4s ease',
        pagetitle: 'pagetitle 4s ease',
      },
      keyframes: {
        logo: {
          '0%': { bottom: '-800px', opacity: '0' },
          '100%': { bottom: '0', opacity: '1' },
        },
        logoButton: {
          '0%': { top: '-50px', opacity: '0' },
          '90%': { top: '-50px', opacity: '0' },
          '100%': { top: '0', opacity: '1' },
        },
        clouds: {
          '0%': { top: '-500px', opacity: '0' },
          '100%': { top: '0', opacity: '1' },
        },
        mountain1: {
          '0%': { bottom: '-200px', },
        },
        mountain2: {
          '0%': { bottom: '-400px', },
        },
        mountain3: {
          '0%': { bottom: '-600px', },
        },
        pagetitle: {
          '0%': { bottom: '-300px', opacity: '0' },
          '100%': { bottom: '0', opacity: '1' },
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
