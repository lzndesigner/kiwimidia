    <div class="w-full bg-white lg:mx-auto">

        <div class="container mx-auto">
            <div class="w-full inline-block text-center">
                <div class="flex justify-center">
                    <div>
                        <h1 class="uppercase text-lg lg:text-4xl font-extrabold tracking-tighter">
                            {{ $result->meta_title }}</h1>
                    </div>
                    @if ($result->featured == 1)
                        <div class="animate-bounce px-2 py-1 w-auto text-yellow-300 text-lg z-10">
                            <i class="fa fa-star"></i>
                        </div>
                    @endif

                </div>
                <div class="w-full inline-block">
                    <div class="content my-3">
                        {!! $result->content !!}
                    </div>
                </div><!-- card -->
            </div><!-- card -->


            @if ($miniaturas != '[]')
                <div class="w-full my-5 block">
                    <div class="grid grid-cols-3 lg:grid-cols-3">
                        <div class="">
                            <div class="h-full w-full lg:h-64 lg:w-64 p-2">
                                <a href="{{ url($result->image) }}" data-fancybox="gallery">
                                    <span><img src="{{ asset($result->image) }}"
                                            class="rounded hover:opacity-75 hover:rounded w-full h-full object-cover transition duration-150 ease-linear"
                                            alt=""></span>
                                </a>
                            </div>
                        </div>

                        @foreach ($miniaturas as $miniatura)
                            <div class="">
                                <div class="h-full lg:h-64 w-full lg:w-64 p-2">
                                    <a href="{{ url($miniatura->image) }}" data-fancybox="gallery">
                                        <span><img src="{{ asset($miniatura->image) }}"
                                                class="rounded hover:opacity-75 hover:rounded w-full h-full object-cover transition duration-150 ease-linear"
                                                alt=""></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @if (isset($result->videos) && $result->videos != null)
                <div class="w-full my-5 block">
                    <div class="grid grid-cols-3 lg:grid-cols-3">
                        @foreach (json_decode($result->videos) as $url_video)
                            <div class="grid-item m-2 p-3 border-rounded border border-black iframe-responsive iframe-grid">
                                <iframe src="{{ str_replace('watch?v=', 'embed/', $url_video) }}"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif

            <div class="mt-10 lg:mt-32 hidden">
                <a href="{{ url('/portfolio') }}"
                    class="inline-block rounded px-4 py-3 bg-black text-white border-2 border-black hover:bg-white hover:text-black  hover:border-black transition duration-100 ease-linear text-sm font-bold uppercase">Ver
                    outros projetos</a>
            </div>
        </div>
        <!-- flex -->
    </div><!-- Serviços -->

    <!-- FancyBox -->
    <link rel="stylesheet" href="{{ asset('/plugins/fancybox/jquery.fancybox.min.css') }}" />


    <!-- FancyBox -->
    <script src="{{ asset('/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        $('[data-fancybox="gallery"]').fancybox({
            selector: '.imglist a:visible'
        });
    </script>

    <script src="{{ asset('/plugins/isotope.pkgd.min.js') }}"></script>
    <script>
        // init Isotope
        $('.grid-portfolio2').isotope({
            // options...
            itemSelector: '.grid-portfolio-item2',
            masonry: {
                columnWidth: 0,
                gutter: 0
            }
        });
    </script>
