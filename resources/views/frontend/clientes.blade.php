@extends('frontend.app')
@section('title', 'Clientes')
@section('background', 'clientes.jpg')

@section('content')
    <!-- BEGIN: Clientes -->
    <section id="clientes" class="w-full bg-white py-20 text-center mx-auto">
        <div class="container px-3 lg:mx-auto my-20">
            <div class="swiper-container grid-clientes">
                <div class="swiper-wrapper">
                    @foreach ($results as $result)
                        <div class="swiper-slide">
                            <a href="{{ $result->web_link }}" target="_Blank"
                                class="text-gray-600 hover:text-black text-center" title="{{ $result->meta_title }}">
                                <img src="{{ asset($result->image) }}" class="h-28 mx-auto"
                                    alt="{{ $result->meta_title }}" title="{{ $result->meta_title }}" />
                                <h2 class="hidden text-sm font-semibold tracking-wider uppercase mt-2 px-3 py-2">
                                    {{ $result->meta_title }}</h2>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div
                    class="swp-button-next absolute z-10 top-1/3 right-0 w-6 h-6 bg-black text-white hover:bg-white hover:text-black rounded-full">
                    <i class="fa fa-angle-right"></i>
                </div>
                <div
                    class="swp-button-prev absolute z-10 top-1/3 left-0 w-6 h-6 bg-black text-white hover:bg-white hover:text-black rounded-full">
                    <i class="fa fa-angle-left"></i>
                </div>
            </div><!-- grid-clientes -->
        </div><!-- container -->
    </section><!-- Clientes -->
@endsection


@section('includeCSS')
    <!-- Swiper -->
    <link rel="stylesheet" href="{{ asset('/plugins/swiper/swiper-bundle.min.css') }}" />
@endsection

@section('includeJS')
    <!-- Swiper -->
    <script src="{{ asset('/plugins/swiper/swiper-bundle.min.js') }}"></script>
    <script>
        var swiper = new Swiper(".grid-clientes", {
            slidesPerView: 1,
            spaceBetween: 10,
            breakpoints: {
                '320': {
                    slidesPerView: 3,
                    spaceBetween: 10,
                    slidesPerGroup: 1,
                },
                '640': {
                    slidesPerView: 4,
                    spaceBetween: 10,
                    slidesPerGroup: 2,
                },
                '1200': {
                    slidesPerView: 6,
                    spaceBetween: 10,
                    slidesPerGroup: 3,
                },
            },
            loop: true,
            autoplay: true,
            freeMode: true,
            navigation: {
                nextEl: ".swp-button-next",
                prevEl: ".swp-button-prev",
            },
        });
    </script>

@endsection
