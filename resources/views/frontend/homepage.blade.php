@extends('frontend.app')
@section('title', $config_site->meta_title)

@section('content')
<!-- BEGIN: Servicos -->
<section id="services" class="w-full bg-black py-14 lg:py-28 text-center mx-auto">
    <h2 class="font-montserrat text-white text-5xl lg:text-7xl font-bold tracking-tighter">Serviços.</h2>

    <div class="container px-4 lg:mx-auto my-10">
        <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-3">
            @foreach ($getServices as $getService)
            <div class="p-6 bg-white rounded">
                <div class="md:block lg:hidden mb-4 lg:mb-0 text-base font-medium text-black uppercase tracking-wider">
                    <a href="{{ url('/servico/' . $getService->slug) }}">{{ $getService->meta_title }}</a>
                </div>
                <div class="flex items-center space-x-4">
                    <div class="flex-shrink-0">
                        <a href="{{ url('/servico/' . $getService->slug) }}"><img class="h-24 w-24 rounded-full" src="{{ asset($getService->image) }}" alt="{{ $getService->meta_title }}"></a>
                    </div>
                    <div class="text-left">
                        <div class="hidden lg:flex text-base font-medium text-black uppercase tracking-wider"><a href="{{ url('/servico/' . $getService->slug) }}">{{ $getService->meta_title }}</a>
                        </div>
                        <p class="text-xs text-gray-500 tracking-wider">{{ $getService->meta_description }}
                        </p>
                    </div>
                </div>
            </div><!-- card -->
            @endforeach
        </div>
    </div><!-- container -->
</section><!-- Serviços -->

<!-- BEGIN: Portfolio -->
<section id="portfolio" class="w-full bg-black py-14 lg:py-28 text-center mx-auto bg-cover object-cover" style="background-attachment: fixed; background-position: top; background-image: url('/galerias/layout/footer.webp');">
    <h2 class="font-montserrat text-5xl lg:text-7xl text-white font-bold tracking-tighter">Portfólio.</h2>

    <div class="container px-3 lg:mx-auto my-10">
        <div class="grid-portfolio grid md:grid-cols-2 lg:grid-cols-3 gap-12 lg:gap-3">
            @foreach ($getPortfolios as $getPortfolio)
            <div class="grid-item {{ str_replace(',', ' ', $getPortfolio->tags) }} inline-block w-full h-72 lg:h-96 rounded relative {{ $getPortfolio->tags == 'videos' || $getPortfolio->tags == 'filmagem_aerea' || $getPortfolio->tags == 'videos,filmagem_aerea' || $getPortfolio->tags == 'filmagem_aerea,videos' ? 'openModalVideo' : 'openModalPortfolio' }}" data-idportfolio="{{ $getPortfolio->id }}" style="background-image: url('{{ asset($getPortfolio->image) }}');background-position: center;background-size: 110%;">
                <a href="javascript:;">
                    <div class="lg:opacity-0 lg:hover:opacity-100 duration-300 absolute lg:rounded bottom-0 w-full lg:inset-0 z-10 py-4 lg:py-0 flex flew-wrap justify-center items-center bg-black bg-opacity-80">
                        <div class="w-4/5 mx-auto">
                            <h2 class="w-full font-montserrat text-white font-semibold text-lg lg:text-3xl tracking-normal uppercase mb-1 lg:mb-3">
                                {{ $getPortfolio->meta_title }}
                            </h2>
                            <div class="w-full font-montserrat text-gray-100 font-light text-sm lg:text-sm overflow-ellipsis overflow-hidden">
                                {{ $getPortfolio->meta_description }}
                            </div>
                        </div><!-- mx-auto -->
                    </div>
                    <!--caption -->
                    @if ($getPortfolio->featured)
                    <div class="animate-bounce absolute px-2 py-1 rounded-r-lg w-auto top-2 left-2 text-yellow-300 text-base z-10">
                        <i class="fa fa-star"></i>
                    </div>
                    @endif
                </a>
            </div><!-- block -->
            @endforeach
        </div><!-- grid-portfolio -->
        <div class="block mt-10">
            <a href="{{ url('/portfolio') }}" class="rounded px-3 py-1 transition ease-linear uppercase border-2 border-black bg-white hover:bg-black hover:text-white text-xs">veja
                mais</a>
        </div>
    </div><!-- container -->
</section><!-- Portfólio -->

<!-- Modal Portfolio -->
<div class="bg-black bg-opacity-80 modal fade flex min-h-screen items-center justify-items-center invisible" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalVideoLabel" aria-hidden="true">
    <div class="flex-grow w-3/4">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="inline-block w-10 rounded bg-black text-gray-200 text-lg px-3 py-1 btnCloseModal" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <form action="" class="form-horizontal" id="form-request">
                    <div class="modal-body" id="form-content">
                        <!-- conteudo -->
                        <!-- conteudo -->
                    </div><!-- modal-body -->
                </form>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: Clientes -->
<section id="clientes" class="w-full bg-white py-20 text-center mx-auto">
    <h2 class="font-montserrat text-5xl lg:text-7xl font-bold tracking-tighter">Clientes.</h2>
    <div class="container px-3 lg:mx-auto my-20">
        <div class="swiper-container grid-clientes">
            <div class="swiper-wrapper">
                @foreach ($getCustomers as $getCustomer)
                <div class="swiper-slide">
                    <a href="{{ $getCustomer->web_link }}" target="_Blank" class="text-gray-600 hover:text-black text-center" title="{{ $getCustomer->meta_title }}">
                        <img src="{{ asset($getCustomer->image) }}" class="h-28 mx-auto" alt="{{ $getCustomer->meta_title }}" title="{{ $getCustomer->meta_title }}" />
                        <h2 class="hidden text-sm font-semibold tracking-wider uppercase mt-2 px-3 py-2">
                            {{ $getCustomer->meta_title }}
                        </h2>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="swp-button-next absolute top-1/3 right-0 w-6 h-6 z-20 bg-black text-white hover:bg-white hover:text-black rounded-full">
                <i class="fa fa-angle-right"></i>
            </div>
            <div class="swp-button-prev absolute top-1/3 left-0 w-6 h-6 z-20 bg-black text-white hover:bg-white hover:text-black rounded-full">
                <i class="fa fa-angle-left"></i>
            </div>
        </div><!-- grid-clientes -->
    </div><!-- container -->
</section><!-- Clientes -->

<!-- BEGIN: COUNTER -->
<section id="counter" class="w-full bg-white py-14 lg:py-28 text-center mx-auto bg-cover object-cover" style="background-attachment: fixed; background-position: top; background-image: url('/storage/uploads/shares/paginas/counter.jpg');">

    <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4">
        <div class="flex flex-col items-center justify-center">
            <div class="text-white"><span class="count font-montserrat font-semibold text-5xl" data-counter="{{ $counters->number_1 }}">
                    {{ $counters->number_1 }}
                </span></div>
            <div class="bg-white my-4 h-px w-14 block"></div>
            <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                {{ $counters->title_1 }}
            </h3>
        </div>
        <!-- col -->
        <div class="flex flex-col items-center justify-center">
            <div class="text-white"><span class="count font-montserrat font-semibold text-5xl" data-counter="{{ $counters->number_2 }}">
                    {{ $counters->number_2 }}
                </span></div>
            <div class="bg-white my-4 h-px w-14 block"></div>
            <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                {{ $counters->title_2 }}
            </h3>
        </div>
        <!-- col -->
        <div class="flex flex-col items-center justify-center">
            <div class="text-white"><span class="count font-montserrat font-semibold text-5xl" data-counter="{{ $counters->number_3 }}">
                    {{ $counters->number_3 }}
                </span></div>
            <div class="bg-white my-4 h-px w-14 block"></div>
            <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                {{ $counters->title_3 }}
            </h3>
        </div>
        <!-- col -->
        <div class="flex flex-col items-center justify-center">
            <div class="text-white"><span class="count font-montserrat font-semibold text-5xl" data-counter="{{ $counters->number_4 }}">
                    {{ $counters->number_4 }}
                </span></div>
            <div class="bg-white my-4 h-px w-14 block"></div>
            <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                {{ $counters->title_4 }}
            </h3>
        </div>
        <!-- col -->
    </div>
</section><!-- end Counter -->

<!-- BEGIN: Contato -->
<section id="contact" class="w-full bg-white py-14 lg:py-20 text-center mx-auto">

    <div class="container lg:w-2/4 px-6 lg:mx-auto my-20">
        <div class="grid lg:grid-cols-2">
            <div class="w-full mb-10 lg:mb-0 order-2 lg:order-1">
                <h2 class="invisible lg:visible text-2xl lg:text-4xl font-bold tracking-tighter text-center lg:text-left">Fale Conosco.
                </h2>
                <div class="m-4">
                    @if (isset($config_site->telephone))
                    @php
                    $retirar = ['(',')','-',' '];
                    $telephone_formated = str_replace($retirar, '', $config_site->telephone);
                    @endphp
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-whatsapp"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://wa.me/55{{ $telephone_formated }}" target="_Blank" class="text-black hover:color-theme">{{ $config_site->telephone }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                    @if (isset($config_site->facebook))
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-facebook"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://facebook.com/{{ $config_site->facebook }}" target="_Blank" class="text-black hover:color-theme">{{ '@' . $config_site->facebook }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                    @if (isset($config_site->instagram))
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-instagram"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://instagram.com/{{ $config_site->instagram }}" target="_Blank" class="text-black hover:color-theme">{{ '@' . $config_site->instagram }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                </div>

                @if (isset($config_site->hour_open))
                <div class="w-full">
                    <h2 class="text-lg lg:text-2xl font-bold tracking-tighter text-center lg:text-left">Horário
                        de Atendimento.
                    </h2>

                    <div class="text-center lg:text-left mt-4">
                        {!! $config_site->hour_open !!}
                    </div>
                </div>
                @endif
            </div>

            <div class="w-full order-1 lg:order-2">
                <h2 class="visible lg:invisible text-4xl lg:text-4xl font-bold tracking-tighter text-center lg:text-left">Fale Conosco.
                </h2>

                <h2 class="text-lg lg:text-2xl font-bold tracking-tighter text-center lg:text-left mb-4">Manda uma mensagem <br>
                    para gente. Vamos adorar!
                </h2>
                <form id="form-mail" method="post" action="{{ url('/contato-sendmail') }}">
                    @csrf
                    <div class="grid grid-col-1 lg:grid-cols-2 lg:space-x-2">
                        <div class="w-full mb-3">
                            <input type="text" name="name" id="name" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Seu nome">
                        </div>
                        <div class="w-full mb-3">
                            <input type="email" name="email" id="email" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Seu e-mail">
                        </div>
                    </div>
                    <div class="grid grid-col-1 lg:grid-cols-2 lg:space-x-2">
                        <div class="w-full mb-3">
                            <input type="text" name="telephone" id="telephone" class="w-full border border-black rounded px-3 py-2 text-gray-800 maskPhone" placeholder="WhatsApp">
                        </div>
                        <div class="w-full mb-3">
                            <input type="text" name="subject" id="subject" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Assunto">
                        </div>
                    </div>
                    <div class="grid">
                        <div class="w-full mb-3">
                            <textarea name="message" id="message" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Sua mensagem" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="block text-left">
                        <button type="button" id="btn-sendmail" value="submit" class="rounded border border-black transition ease-linear bg-white text-black hover:bg-black hover:text-white uppercase text-sm font-medium px-4 py-2">Enviar
                            Mensagem</button>
                    </div>
                </form>
            </div>


        </div>
    </div>

</section><!-- Contato -->

@endsection

@section('includeCSS')
<!-- Include SweetAlert -->
<link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">

<!-- Swiper -->
<link rel="stylesheet" href="{{ asset('/plugins/swiper/swiper-bundle.min.css') }}" />
@endsection

@section('includeJS')
<!-- Include SweetAlert -->
<script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>

<!-- Swiper -->
<script src="{{ asset('/plugins/swiper/swiper-bundle.min.js') }}"></script>
<script>
    var swiper = new Swiper(".grid-clientes", {
        slidesPerView: 1,
        spaceBetween: 10,
        breakpoints: {
            '320': {
                slidesPerView: 3,
                spaceBetween: 10,
                slidesPerGroup: 1,
            },
            '640': {
                slidesPerView: 4,
                spaceBetween: 10,
                slidesPerGroup: 2,
            },
            '1200': {
                slidesPerView: 6,
                spaceBetween: 10,
                slidesPerGroup: 3,
            },
        },
        loop: true,
        autoplay: true,
        freeMode: true,
        navigation: {
            nextEl: ".swp-button-next",
            prevEl: ".swp-button-prev",
        },
    });
</script>

<script src="{{ asset('/plugins/isotope.pkgd.min.js') }}"></script>
<script>
    // init Isotope
    $('.grid-portfoli2o').isotope({
        // options...
        itemSelector: '.grid-item',
        masonry: {
            columnWidth: 0,
            gutter: 0
        }
    });
</script>



<script>
    $(document).on('click', '#btn-sendmail', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        $(this).attr('disabled', true);

        var url = "{{ url('/contato-sendmail') }}";
        var method = 'POST';

        var data = $('#form-mail').serialize();
        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                Swal.fire({
                    text: data,
                    icon: 'success',
                    showClass: {
                        popup: 'animate_animated animate_backInUp'
                    },
                    onClose: () => {
                        // Loading page listagem
                        $('#form-mail')[0].reset();
                        $('#btn-sendmail').attr('disabled', false);
                    }
                });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }

                $('#btn-sendmail').attr('disabled', false);
            }
        });
    });
</script>
<script src="{{ asset('/plugins/jquery.mask.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.maskPhone').mask("(99) 99999-9999");
        $('.maskPhone').on("blur", function() {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 3) {
                var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                var lastfour = move + last;
                var first = $(this).val().substr(0, 9);

                $(this).val(first + '-' + lastfour);
            }
        });;

    });
</script>
<!-- counter Js -->
<script src="{{ asset('/plugins/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('/plugins/jquery.counterup.min.js?1') }}"></script>
<script>
    $('.count').counterUp({
        delay: 10,
        time: {
            {
                $counters - > time
            }
        }
    });
</script>

<script>
    $('.gotoPortfolio').click(function() {
        e.preventDefault();

        $('html,body').animate({
            scrollTop: $("#portfolio").offset().top
        }, 'slow');
    });
</script>

<script src="{{ asset('/plugins/template.js') }}"></script>
@endsection