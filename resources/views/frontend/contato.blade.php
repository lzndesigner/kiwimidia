@extends('frontend.app')
@section('title', 'Contato')
@section('background', 'page_contato.jpg?2')
@section('background-position', 'center')

@section('content')
<!-- BEGIN: Contato -->
<section id="contact" class="w-full bg-white py-14 lg:py-20 text-center mx-auto">

    <div class="container lg:w-2/4 px-6 lg:mx-auto my-20">
        <div class="grid lg:grid-cols-2">
            <div class="w-full mb-10 lg:mb-0 order-2 lg:order-1">
                <h2 class="invisible lg:visible text-2xl lg:text-4xl font-bold tracking-tighter text-center lg:text-left">Fale Conosco.
                </h2>
                <div class="m-4">
                    @if (isset($config_site->telephone))
                    @php
                        $retirar = ['(',')','-',' '];
                        $telephone_formated = str_replace($retirar, '', $config_site->telephone);
                    @endphp
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-whatsapp"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://wa.me/55{{ $telephone_formated }}" target="_Blank" class="text-black hover:color-theme">{{ $config_site->telephone }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                    @if (isset($config_site->facebook))
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-facebook"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://facebook.com/{{ $config_site->facebook }}" target="_Blank" class="text-black hover:color-theme">{{ '@' . $config_site->facebook }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                    @if (isset($config_site->instagram))
                    <div class="flex flex-wrap justify-center lg:justify-start my-6">
                        <div class="flex-shrink-0 mr-3">
                            <span class="w-8 h-8 leading-8 block rounded-full bg-black text-white"><i class="fab fa-instagram"></i></span>
                        </div>
                        <div>
                            <h3 class="leading-8 font-medium">
                                <a href="https://instagram.com/{{ $config_site->instagram }}" target="_Blank" class="text-black hover:color-theme">{{ '@' . $config_site->instagram }}</a>
                            </h3>
                        </div>
                    </div><!-- item -->
                    @endif
                </div>

                @if (isset($config_site->hour_open))
                <div class="w-full">
                    <h2 class="text-lg lg:text-2xl font-extrabold tracking-tighter text-center lg:text-left">Horário
                        de Atendimento.
                    </h2>

                    <div class="text-center lg:text-left mt-4">
                        {!! $config_site->hour_open !!}
                    </div>
                </div>
                @endif
            </div>

            <div class="w-full order-1 lg:order-2">
                <h2 class="visible lg:invisible text-4xl lg:text-4xl font-bold tracking-tighter text-center lg:text-left">Fale Conosco.
                </h2>
                <h2 class="text-lg lg:text-2xl font-extrabold tracking-tighter text-center lg:text-left mb-4">Manda uma mensagem <br>
                    para gente. Vamos adorar!
                </h2>
                <form id="form-mail" method="post" action="{{ url('/contato-sendmail') }}">
                    @csrf
                    <div class="grid grid-col-1 lg:grid-cols-2 lg:space-x-2">
                        <div class="w-full mb-3">
                            <input type="text" name="name" id="name" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Seu nome">
                        </div>
                        <div class="w-full mb-3">
                            <input type="email" name="email" id="email" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Seu e-mail">
                        </div>
                    </div>
                    <div class="grid grid-col-1 lg:grid-cols-2 lg:space-x-2">
                        <div class="w-full mb-3">
                            <input type="text" name="telephone" id="telephone" class="w-full border border-black rounded px-3 py-2 text-gray-800 maskPhone" placeholder="WhatsApp">
                        </div>
                        <div class="w-full mb-3">
                            <input type="text" name="subject" id="subject" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Assunto">
                        </div>
                    </div>
                    <div class="grid grid-cols-1">
                        <div class="w-full mb-3">
                            <textarea name="message" id="message" class="w-full border border-black rounded px-3 py-2 text-gray-800" placeholder="Sua mensagem" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="block text-left">
                        <button type="button" id="btn-sendmail" value="submit" class="rounded border border-black transition ease-linear bg-white text-black hover:bg-black hover:text-white uppercase text-sm font-medium px-4 py-2">Enviar
                            Mensagem</button>
                    </div>
                </form>
            </div>


        </div>
    </div>

</section><!-- Contato -->
@endsection


@section('includeCSS')
<!-- Include SweetAlert -->
<link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
<script src="{{ asset('/plugins/jquery.mask.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.maskPhone').mask("(99) 99999-9999");
        $('.maskPhone').on("blur", function() {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 3) {
                var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                var lastfour = move + last;
                var first = $(this).val().substr(0, 9);

                $(this).val(first + '-' + lastfour);
            }
        });;

    });
</script>

<!-- Include SweetAlert -->
<script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
<script>
    $(document).on('click', '#btn-sendmail', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        $(this).attr('disabled', true);

        var url = "{{ url('/contato-sendmail') }}";
        var method = 'POST';

        var data = $('#form-mail').serialize();
        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                Swal.fire({
                    text: data,
                    icon: 'success',
                    showClass: {
                        popup: 'animate_animated animate_backInUp'
                    },
                    onClose: () => {
                        // Loading page listagem
                        $('#form-mail')[0].reset();
                        $('#btn-sendmail').attr('disabled', false);
                    }
                });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }

                $('#btn-sendmail').attr('disabled', false);
            }
        });
    });
</script>
@endsection