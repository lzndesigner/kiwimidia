<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $config_site->name_site }}</title>

    <link href="{{ asset('css/app.css?2') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;600;700;900&display=swap" rel="stylesheet">
    <link href="{{ asset('/plugins/fontawesome/css/all.min.css?2') }}" rel="stylesheet">

    <!-- Include Favicon -->
    <link rel="shortcut icon" href="{{ asset('/galerias/favicon.ico') }}" />

    @yield('includeCSS')

    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-42R9D40MW8"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-42R9D40MW8');
    </script>
</head>

<body class="page-{{ collect(request()->segments())->last() == null ? 'home' : collect(request()->segments())->last() }} min-h-screen bg-white text-black">
    <div id="app" class="relative">

        <section id="services" class="w-full min-h-screen bg-gray-100 py-14 lg:py-20 text-center mx-auto">
            <div class="container px-4 lg:mx-auto my-10">
                <h1 class="font-montserrat text-2xl lg:text-3xl text-black"><b>CHAVE PIX</b></h1>
                <h1 class="font-montserrat text-2xl lg:text-3xl text-black" id="description_copy">{{$result->description}}</h1>
            </div><!-- container -->
        </section><!-- Serviços -->
    </div><!-- #app -->

    <script src="{{ asset('/backend/js/jquery-3.6.0.js') }}"></script>
    <script>
        /* TEMPLATE */
        $(document).ready(function() {
            $('.openMenu').on('click', function(e) {
                e.preventDefault();
                $('#menuSidebar').toggleClass('invisible');
                $('#menuSidebar').toggleClass('-left-full');
            });
        });

        // $('#menu-header ul li a, #menu-mobile ul li a, #menu-footer ul li a').on('click', function(e) {
        //     var targetSec = $(this).data('target');
        //     $('html, body').animate({
        //         scrollTop: $('#' + targetSec).offset().top
        //     }, 750);
        // });

        $('.gotoPortfolio').on('click', function(e) {
            $('html, body').animate({
                scrollTop: $('#portfolio').offset().top
            }, 650);
        });

        $('.gotoTop').on('click', function(e) {
            $('html, body').animate({
                scrollTop: $('#header').offset().top
            }, 650);
        });

        var $win = $(window);
        $win.on('scroll', function() {
            if ($win.width() >= 992) {
                if ($win.scrollTop() > 50) {
                    $('#menu-header').removeClass('invisible').addClass('is-sticky');
                    $('#logo-header').removeClass('invisible').removeClass('is-logo');
                } else {
                    $('#logo-header').addClass('invisible').addClass('is-logo');
                    $('#menu-header').addClass('invisible').removeClass('is-sticky');
                }
            }
        });
    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            Swal.fire({
                text: 'Chave PIX copiada com Sucesso!',
                icon: 'success',
                showClass: {
                    popup: 'animate_animated animate_backInUp'
                }
            }).then(function() {
                copyToClipboardShare('#description_copy');
            });
        });

        function copyToClipboardShare(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text().replace("\n", "\n\n")).select();
            document.execCommand("copy");
            $temp.remove();
        }
    </script>
</body>

</html>