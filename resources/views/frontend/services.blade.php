@extends('frontend.app')
@section('title', 'Serviços')
@section('background', 'servicos.jpg')

@section('content')
<section id="services" class="w-full bg-gray-100 py-14 lg:py-20 text-center mx-auto">
    <div class="container px-4 lg:mx-auto my-10">
        <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-3">
            @foreach ($results as $result)
                <div class="p-6 bg-white rounded">
                    <div
                        class="md:block lg:hidden mb-4 lg:mb-0 text-base font-medium text-black uppercase tracking-wider">
                        <a href="{{ url('/servico/' . $result->slug) }}">{{ $result->meta_title }}</a>
                    </div>
                    <div class="flex items-center space-x-4">
                        <div class="flex-shrink-0">
                            <a href="{{ url('/servico/' . $result->slug) }}"><img class="h-24 w-24 rounded-full"
                                    src="{{ asset($result->image) }}" alt="{{ $result->meta_title }}"></a>
                        </div>
                        <div class="text-left">
                            <div class="hidden lg:flex text-base font-medium text-black uppercase tracking-wider"><a
                                    href="{{ url('/servico/' . $result->slug) }}">{{ $result->meta_title }}</a>
                            </div>
                            <p class="text-xs text-gray-500 tracking-wider">{{ $result->meta_description }}
                            </p>
                        </div>
                    </div>
                </div><!-- card -->
            @endforeach
        </div>
    </div><!-- container -->
</section><!-- Serviços -->
@endsection


@section('includeCSS')
@endsection

@section('includeJS')
<script src="{{ asset('/plugins/isotope.pkgd.min.js') }}"></script>
<script>
    // init Isotope
    $('.grid-portfolio').isotope({
        // options...
        itemSelector: '.grid-item',
        masonry: {
            columnWidth: 0,
            gutter: 0
        }
    });

</script>
@endsection