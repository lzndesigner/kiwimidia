<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') | {{ $config_site->name_site }}</title>

    <link href="{{ asset('css/app.css?2') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;600;700;900&display=swap"
    rel="stylesheet">
    <link href="{{ asset('/plugins/fontawesome/css/all.min.css?2') }}" rel="stylesheet">
    
    <!-- Include Favicon -->
    <link rel="shortcut icon" href="{{ asset('/galerias/favicon.ico') }}" />
    
    @yield('includeCSS')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-42R9D40MW8"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-42R9D40MW8');
    </script>
</head>

<body
    class="page-{{ collect(request()->segments())->last() == null ? 'home' : collect(request()->segments())->last() }} min-h-screen bg-white text-black">
    <div id="app" class="relative">


        @include('frontend.includes.menu-sidebar')

        @if (collect(request()->segments())->last() == null)
            <header id="header" class="relative overflow-hidden w-full min-h-screen bg-black text-white">
                <div
                    class="absolute w-full top-0 sm:top-0 md:top-0 lg:top-0 xl:top-0 left-0 z-1 text-center animate-clouds">
                    <img src="{{ asset('/galerias/layout/clouds.webp') }}" alt="" class="w-full">
                </div>
                <div
                    class="pt-0 sm:pt-0 md:pt-0 lg:pt-0 pb-11 lg:pb-0 min-h-screen flex flex-col justify-center items-center mx-auto space-x-4 z-2 relative">
                    <a href="{{ url('/') }}" class="w-7/12 lg:w-3/12 relative animate-logo"><img
                            src="{{ asset('/galerias/logo_kiwimidia_branco.png') }}" alt="Kiwimidia"></a>
                </div>

                <div
                    class="absolute bottom-2 sm:bottom-0 md:bottom-8 lg:bottom-8 xl:bottom-10 -left-96 sm:left-0 z-4 animate-mountain1">
                    <img src="{{ asset('/galerias/layout/mountain1.webp') }}" alt="" class="transform scale-150">
                </div>
                <div
                    class="absolute bottom-3 sm:bottom-2 md:bottom-4 lg:bottom-7 xl:bottom-2 -left-96 sm:left-0 z-3 animate-mountain2">
                    <img src="{{ asset('/galerias/layout/mountain2.webp') }}" alt="">
                </div>
                <div
                    class="absolute bottom-5 sm:bottom-4 md:bottom-8 lg:bottom-10 xl:bottom-10 -left-96 sm:left-0 z-2 animate-mountain3">
                    <img src="{{ asset('/galerias/layout/mountain3.webp') }}" alt="">
                </div>

                <div
                    class="absolute inset-0 top-24 z-20 min-h-screen flex flex-col justify-center items-center invisible">
                    <a href="#"
                        class="gotoPortfolio relative z-20 px-3 md:px-6 py-2 md:py-3 border-2 text-sm md:text-base uppercase tracking-wide transition ease-linear border-gray-300 text-gray-300 hover:text-black hover:bg-white hover:border-white rounded animate-logoButton">Portfólio</a>
                </div>

                <div id="button-menu" class="visible lg:invisible fixed top-5 left-5 z-20">
                    <a href="#"
                        class="p-2 text-2xl transition ease-linear bg-black text-white hover:bg-white hover:text-black openMenu"><i
                            class="fa fa-th" aria-hidden="true"></i></a>
                </div>

                <div id="menu-header"
                    class="invisible lg:visible absolute bg-black px-2 py-4 text-right top-0 right-0 w-full z-20 flex justify-between">
                    <div id="logo-header" class="invisible w-40">
                        <a href="{{ url('/') }}"><img src="{{ asset('/galerias/logo_kiwimidia_branco.png') }}"
                                alt="Kiwimidia"></a>
                    </div>
                    <nav>
                        <ul>
                            @include('frontend.includes.menu')
                        </ul>
                    </nav>
                </div>


            </header><!-- home-content -->
        @else
            <header id="header"
                class="relative overflow-hidden w-full h-subpage bg-black text-white bg-cover object-cover"
                style="background-attachment: fixed; background-image: url('/storage/uploads/shares/paginas/@yield('background')'); background-position:@yield('background-position', 'top');">

                <div class="h-full flex flex-col justify-center items-center mx-auto space-x-4 z-2 relative">
                    <div class="relative animate-pagetitle text-center">
                        <h1 class="font-montserrat text-5xl lg:text-7xl text-white font-bold tracking-tighter">
                            @yield('title').</h1>
                        <h3 class="mt-5 text-lg lg:text3x1 text-gray-300 font-semibold tracking-wider">
                            @yield('subtitulo')</h3>
                    </div>
                </div>

                <div id="button-menu" class="visible md:invisible fixed top-5 left-5 z-10">
                    <a href="#"
                        class="p-2 text-2xl transition ease-linear bg-black text-white hover:bg-white hover:text-black openMenu"><i
                            class="fa fa-bars" aria-hidden="true"></i></a>
                </div>

                <div id="menu-header"
                    class="invisible md:visible absolute bg-black px-2 py-4 text-right top-0 right-0 w-full z-20 flex justify-between">
                    <div id="logo-header-others" class="w-40">
                        <a href="{{ url('/') }}"><img src="{{ asset('/galerias/logo_kiwimidia_branco.png') }}"
                                alt="Kiwimidia"></a>
                    </div>
                    <nav>
                        <ul class="font-montserrat">
                            @include('frontend.includes.menu')
                        </ul>
                    </nav>
                </div>
            </header><!-- home-content -->
        @endif

        @yield('content')

        <div id="footer" class="w-full py-28 pb-5 bg-black text-white object-cover"
            style="background-attachment: fixed; background-position: top; background-image: url('/storage/uploads/shares/paginas/footer_cortado.jpg');">
            <div class="flex justify-center items-center mx-auto space-x-4">
                <img src="{{ asset('/galerias/logo_kiwimidia_branco.png') }}" alt="Kiwimidia"
                    class="w-8/12 md:w-2/12">
            </div><!-- end logo Footer -->

            <div class="flex flex-col lg:flex-wrap justify-center items-center mt-7 mx-auto space-x-4">
                <nav id="menu-footer">
                    <ul>
                        <li class="w-28 lg:w-32 lg:inline-block text-center">
                            <a href="{{ url('/') }}"
                                class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Início</a>
                        </li>
                        <li class="w-28 lg:w-32 lg:inline-block text-center">
                            <a href="{{ url('/servicos') }}"
                                class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Serviços</a>
                        </li>
                        <li class="w-28 lg:w-32 lg:inline-block text-center">
                            <a href="{{ url('/portfolio') }}"
                                class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Portfólio</a>
                        </li>
                        <li class="w-28 lg:w-32 lg:inline-block text-center">
                            <a href="{{ url('/clientes') }}"
                                class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Clientes</a>
                        </li>
                        @if (isset($getAbout))
                            <li class="w-28 lg:w-32 lg:inline-block text-center">
                                <a href="{{ url('/sobre') }}"
                                    class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Sobre</a>
                            </li>
                        @endif
                        <li class="w-28 lg:w-32 lg:inline-block text-center">
                            <a href="{{ url('/contato') }}"
                                class="block text-xs px-2 py-3 rounded transition ease-linear text-white hover:text-gray-100">Contato</a>
                        </li>
                    </ul>
                </nav>
            </div><!-- end Menu Footer -->
            <div class="flex justify-center items-center mt-5 mx-auto space-x-4">
                <ul class="">
                    @if (isset($config_site->twitter))
                        <li class="inline-block">
                            <a href="https://twitter.com/{{ $config_site->twitter }}" target="_Blank"
                                class="block px-2 py-2 transition ease-linear text-white hover:text-opacity-50 text-sm"><i
                                    class="fab fa-twitter"></i></a>
                        </li>
                    @endif
                    @if (isset($config_site->facebook))
                        <li class="inline-block">
                            <a href="https://facebook.com/{{ $config_site->facebook }}" target="_Blank"
                                class="block px-2 py-2 transition ease-linear text-white hover:text-opacity-50 text-sm"><i
                                    class="fab fa-facebook"></i></a>
                        </li>
                    @endif
                    @if (isset($config_site->instagram))
                        <li class="inline-block">
                            <a href="https://instagram.com/{{ $config_site->instagram }}" target="_Blank"
                                class="block px-2 py-2 transition ease-linear text-white hover:text-opacity-50 text-sm"><i
                                    class="fab fa-instagram"></i></a>
                        </li>
                    @endif
                    @if (isset($config_site->whatsapp))
                    @php
                        $retirar = ['(',')','-',' '];
                        $whatsApp_formated = str_replace($retirar, '', $config_site->whatsapp);
                    @endphp
                        <li class="inline-block">
                            <a href="https://wa.me/55{{ $whatsApp_formated }}"
                                target="_Blank"
                                class="block px-2 py-2 transition ease-linear text-white hover:text-opacity-50 text-sm"><i
                                    class="fab fa-whatsapp"></i></a>
                        </li>
                    @endif
                </ul>
            </div><!-- end Redes Sociais -->

            <div class="w-full mx-auto mt-32 text-center">
                <a href="#"
                    class="border border-white transition ease-linear hover:bg-white hover:text-black px-4 py-2 animate-bounce gotoTop"><i
                        class="fa fa-angle-up"></i></a>

                <p class="mt-4 text-sm text-gray-200"><small>Todos os Direitos reservados à <b>Kiwimidia
                            {{ date('Y') }}©</b>. <br> CNPJ: {{ $config_site->cnpj }} </small></p>
            </div>

        </div><!-- footer -->

    </div><!-- #app -->

    <script src="{{ asset('/backend/js/jquery-3.6.0.js') }}"></script>
    <script>
        /* TEMPLATE */
        $(document).ready(function() {
            $('.openMenu').on('click', function(e) {
                e.preventDefault();
                $('#menuSidebar').toggleClass('invisible');
                $('#menuSidebar').toggleClass('-left-full');
            });
        });

        // $('#menu-header ul li a, #menu-mobile ul li a, #menu-footer ul li a').on('click', function(e) {
        //     var targetSec = $(this).data('target');
        //     $('html, body').animate({
        //         scrollTop: $('#' + targetSec).offset().top
        //     }, 750);
        // });

        $('.gotoPortfolio').on('click', function(e) {
            $('html, body').animate({
                scrollTop: $('#portfolio').offset().top
            }, 650);
        });

        $('.gotoTop').on('click', function(e) {
            $('html, body').animate({
                scrollTop: $('#header').offset().top
            }, 650);
        });

        var $win = $(window);
        $win.on('scroll', function() {
            if ($win.width() >= 992) {
                if ($win.scrollTop() > 50) {
                    $('#menu-header').removeClass('invisible').addClass('is-sticky');
                    $('#logo-header').removeClass('invisible').removeClass('is-logo');
                } else {
                    $('#logo-header').addClass('invisible').addClass('is-logo');
                    $('#menu-header').addClass('invisible').removeClass('is-sticky');
                }
            }
        });
    </script>
    @yield('includeJS')
</body>

</html>
