<div id="menuSidebar"
    class="z-50 invisible transition ease-linear -left-full fixed w-80 min-h-screen bg-white text-black p-4">
    <div class="p-3">
        <a href="#" class="openMenu px-2 py-2 rounded transition ease-linear hover:bg-gray-100"><i
                class="fa fa-times"></i></a>
    </div>
    <div class="p-3">
        <img src="{{ asset('/galerias/logo_kiwimidia_preto.png') }}" alt="Kiwimidia" class="w-9/12">
    </div>
    <div class="py-3">
        <nav id="menu-mobile">
            <ul>
                <li>
                    <a href="{{ url('/') }}"
                        class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Início</a>
                </li>
                <li>
                    <a href="{{ url('/servicos') }}"
                        class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Serviços</a>
                </li>
                <li>
                    <a href="{{ url('/portfolio') }}"
                        class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Portfólio</a>
                </li>
                <li>
                    <a href="{{ url('/clientes') }}"
                        class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Clientes</a>
                </li>
                @if (isset($getAbout))
                    <li>
                        <a href="{{ url('/sobre') }}"
                            class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Sobre</a>
                    </li>
                @endif
                <li>
                    <a href="{{ url('/contato') }}"
                        class="block px-4 py-3 rounded transition ease-linear hover:bg-gray-100">Contato</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="py-3 px-2 text-gray-500 font-xs">
        <small>Todos os Direitos reservados à <br> <b>Kiwimidia {{ date('Y') }}©</b>.</small>
    </div>

    <div class="flex justify-start items-start space-x-4">
        <ul class="">
            @if (isset($config_site->twitter))
                <li class="inline-block">
                    <a href="https://twitter.com/{{ $config_site->twitter }}" target="_Blank"
                        class="block px-2 py-2 transition ease-linear text-black hover:text-opacity-50 text-base"><i
                            class="fa fa-twitter"></i></a>
                </li>
            @endif
            @if (isset($config_site->facebook))
                <li class="inline-block">
                    <a href="https://facebook.com/{{ $config_site->facebook }}" target="_Blank"
                        class="block px-2 py-2 transition ease-linear text-black hover:text-opacity-50 text-base"><i
                            class="fa fa-facebook"></i></a>
                </li>
            @endif
            @if (isset($config_site->instagram))
                <li class="inline-block">
                    <a href="https://instagram.com/{{ $config_site->instagram }}" target="_Blank"
                        class="block px-2 py-2 transition ease-linear text-black hover:text-opacity-50 text-base"><i
                            class="fa fa-instagram"></i></a>
                </li>
            @endif
            @if (isset($config_site->whatsapp))
                <li class="inline-block">
                    <a href="https://api.whatsapp.com/send?phone=55{{ $config_site->whatsapp }}" target="_Blank"
                        class="block px-2 py-2 transition ease-linear text-black hover:text-opacity-50 text-base"><i
                            class="fa fa-whatsapp"></i></a>
                </li>
            @endif
        </ul>
    </div><!-- end Redes Sociais -->
</div><!-- menuSiderbar -->
