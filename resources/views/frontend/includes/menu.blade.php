<li class="w-28 lg:w-32 text-center inline-block">
    <a href="{{ url('/') }}"
        class="text-xs px-2 py-3 rounded transition ease-linear">Início</a>
</li>
<li class="w-28 lg:w-32 text-center inline-block">
    <a href="{{ url('/servicos') }}"
        class="text-xs px-2 py-3 rounded transition ease-linear">Serviços</a>
</li>
<li class="w-28 lg:w-32 text-center inline-block">
    <a href="{{ url('/portfolio') }}"
        class="text-xs px-2 py-3 rounded transition ease-linear">Portfólio</a>
</li>
<li class="w-28 lg:w-32 text-center inline-block">
    <a href="{{ url('/clientes') }}"
        class="text-xs px-2 py-3 rounded transition ease-linear">Clientes</a>
</li>
@if (isset($getAbout))
    <li class="w-28 lg:w-32 text-center inline-block">
        <a href="{{ url('/sobre') }}"
            class="text-xs px-2 py-3 rounded transition ease-linear">Sobre</a>
    </li>
@endif
<li class="w-28 lg:w-32 text-center inline-block">
    <a href="{{ url('/contato') }}"
        class="text-xs px-2 py-3 rounded transition ease-linear">Contato</a>
</li>
