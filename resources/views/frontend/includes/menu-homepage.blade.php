<nav id="menu-header" class="invisible md:visible fixed bg-black px-2 py-4 text-right top-0 right-0 w-full z-20">
    <ul>
        <li class="w-32 text-center inline-block">
            <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="header">Início</a>
        </li>
        <li class="w-32 text-center inline-block">
            <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="services">Serviços</a>
        </li>
        <li class="w-32 text-center inline-block">
            <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="portfolio">Portfólio</a>
        </li>
        <li class="w-32 text-center inline-block">
            <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="clientes">Clientes</a>
        </li>
        @if (isset($getAbout))
            <li class="w-32 text-center inline-block">
                <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="about">Sobre</a>
            </li>
        @endif
        <li class="w-32 text-center inline-block">
            <a href="#" class="text-xs px-2 py-3 rounded transition ease-linear" data-target="contact">Contato</a>
        </li>
    </ul>
</nav>
