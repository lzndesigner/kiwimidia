@extends('frontend.app')
@section('title', 'Sobre')
@section('subtitle', 'Conheça nossa História')
@section('background', 'sobre.jpg')
@section('background-position', 'bottom')

@section('content')
    <!-- BEGIN: Sobre -->
    <section id="about" class="w-full bg-white py-14 lg:py-20 mx-auto">

        <div class="w-3/4 mx-auto py-3 lg:py-8">
            {!! $getAbout->content !!}
        </div>

        @if (count($getTeams) > 0)
            <div class="my-10">
                <h2 class="font-montserrat text-center text-2xl lg:text-4xl text-black font-bold tracking-tighter">
                    Equipe.</h2>

                <div class="w-8/12 mx-auto">
                    <div class="grid grid-cols-1 lg:grid-cols-3 gap-4 mt-5 text-center">
                        @foreach ($getTeams as $team)
                            <div>
                                <img src="{{ asset($team->image) }}" alt="{{ $team->name }}" class="w-full rounded p-3">
                                <h3
                                    class="font-montserrat text-sm lg:text-base text-black font-semibold tracking-tighter uppercase">
                                    {{ $team->name }}</h3>

                                @if ($team->description)
                                    <p class="text-sm text-gray-500 my-2">{{ $team->description }}</p>
                                @endif

                                <div class="flex justify-center items-center space-x-4 mt-3">
                                    @if ($team->facebook)
                                        <div><a href="https://facebook.com/{{ $team->facebook }}" target="_Blank"
                                                class="text-sm transition ease-linear text-gray-500 hover:text-black"><i
                                                    class="fa fa-facebook"></i></a></div>
                                    @endif
                                    @if ($team->instagram)
                                        <div><a href="https://instagram.com/{{ $team->instagram }}" target="_Blank"
                                                class="text-sm transition ease-linear text-gray-500 hover:text-black"><i
                                                    class="fa fa-instagram"></i></a></div>
                                    @endif
                                    @if ($team->whatsapp)
                                        <div><a href="https://api.whatsapp.com/send?phone=55{{ $team->whatsapp }}"
                                                target="_Blank"
                                                class="text-sm transition ease-linear text-gray-500 hover:text-black"><i
                                                    class="fa fa-whatsapp"></i></a></div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </section><!-- Sobre -->


    <!-- BEGIN: COUNTER -->
    <section id="counter" class="w-full bg-white py-14 lg:py-28 text-center mx-auto bg-cover object-cover"
        style="background-attachment: fixed; background-position: top; background-image: url('/storage/uploads/shares/paginas/counter.jpg');">

        <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4">
            <div class="flex flex-col items-center justify-center">
                <div class="text-white"><span class="count font-montserrat font-semibold text-5xl"
                        data-counter="{{ $counters->number_1 }}">
                        {{ $counters->number_1 }}
                    </span></div>
                <div class="bg-white my-4 h-px w-14 block"></div>
                <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                    {{ $counters->title_1 }}</h3>
            </div>
            <!-- col -->
            <div class="flex flex-col items-center justify-center">
                <div class="text-white"><span class="count font-montserrat font-semibold text-5xl"
                        data-counter="{{ $counters->number_2 }}">
                        {{ $counters->number_2 }}
                    </span></div>
                <div class="bg-white my-4 h-px w-14 block"></div>
                <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                    {{ $counters->title_2 }}</h3>
            </div>
            <!-- col -->
            <div class="flex flex-col items-center justify-center">
                <div class="text-white"><span class="count font-montserrat font-semibold text-5xl"
                        data-counter="{{ $counters->number_3 }}">
                        {{ $counters->number_3 }}
                    </span></div>
                <div class="bg-white my-4 h-px w-14 block"></div>
                <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                    {{ $counters->title_3 }}</h3>
            </div>
            <!-- col -->
            <div class="flex flex-col items-center justify-center">
                <div class="text-white"><span class="count font-montserrat font-semibold text-5xl"
                        data-counter="{{ $counters->number_4 }}">
                        {{ $counters->number_4 }}
                    </span></div>
                <div class="bg-white my-4 h-px w-14 block"></div>
                <h3 class="font-montserrat text-sm text-white font-semibold tracking-wide uppercase">
                    {{ $counters->title_4 }}</h3>
            </div>
            <!-- col -->
        </div>
    </section><!-- end Counter -->
@endsection


@section('includeCSS')
@endsection

@section('includeJS')
    <!-- counter Js -->
    <script src="{{ asset('/plugins/jquery.waypoints.min.js?3') }}"></script>
    <script src="{{ asset('/plugins/jquery.counterup.min.js?4') }}"></script>
    <script>
        $('.count').counterUp({
            delay: 10,
            time: {{ $counters->time }}
        });
    </script>
@endsection
