@extends('frontend.app')
@section('title', $result->meta_title)
@section('background', 'portfolio.jpg')
@section('background-position', 'center')

@section('content')
    <div class="w-full bg-white py-3 lg:py-20 px-4 lg:mx-auto text-center">

        <div class="container mx-auto my-10">
            <div class="w-full inline-block">
                <div class="flex flex-col-reverse lg:flex-row items-start justify-center">
                    <div class="w-full lg:w-2/4 lg:px-6 flex-initial ">
                        <a href="{{ url($result->image) }}" class="block" data-fancybox="gallery">
                            <img src="{{ asset($result->image) }}" alt="{{ $result->meta_title }}" class="rounded">
                        </a>
                    </div>
                    <div class="flex-initial w-full lg:w-2/4 lg:px-6 text-left">
                        @if ($result->featured == 1)
                            <div class="animate-bounce px-2 py-1 w-auto text-yellow-300 text-lg z-10">
                                <i class="fa fa-star"></i>
                            </div>
                        @endif

                        <div class="content my-3 lg:my-10">
                            {!! $result->content !!}
                        </div>
                    </div>
                </div><!-- card -->
            </div><!-- card -->


            @if ($miniaturas != '[]')
                <div class="w-full my-5 block">
                    <h3 class="uppercase text-lg lg:text-3xl text-left font-extrabold tracking-tighter">Miniaturas.</h3>
                    <div class="grid grid-portfolio">
                        @foreach ($miniaturas as $miniatura)
                            <div class="grid-portfolio-item">
                                <div class="h-64 w-64 p-2">
                                    <a href="{{ url($miniatura->image) }}" data-fancybox="gallery">
                                        <span class="p-2"><img src="{{ asset($miniatura->image) }}"
                                                class="rounded hover:opacity-75 hover:rounded w-full h-full object-cover transition duration-150 ease-linear"
                                                alt=""></span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @if (isset($result->videos) && $result->videos != null)
            <div class="w-full my-20 block">
                <h3 class="uppercase text-lg lg:text-3xl text-left font-extrabold tracking-tighter">Vídeos.</h3>

                <div class="grid grid-cols-3">
                    @foreach (json_decode($result->videos) as $url_video)
                        <div class="grid-item m-2 p-3 border-rounded border border-black iframe-responsive">
                            <iframe src="{{ str_replace('watch?v=', 'embed/', $url_video) }}" title="YouTube video player"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif

            <div class="mt-10 lg:mt-32">
                <a href="{{ url('/portfolio') }}"
                    class="inline-block rounded px-4 py-3 bg-black text-white border-2 border-black hover:bg-white hover:text-black  hover:border-black transition duration-100 ease-linear text-sm font-bold uppercase">Ver
                    outros projetos</a>
            </div>
        </div>
        <!-- flex -->
    </div><!-- Serviços -->

@endsection

@section('includeCSS')
    <!-- FancyBox -->
    <link rel="stylesheet" href="{{ asset('/plugins/fancybox/jquery.fancybox.min.css') }}" />

@endsection
@section('includeJS')
    <!-- FancyBox -->
    <script src="{{ asset('/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        $('[data-fancybox="gallery"]').fancybox({
            selector: '.imglist a:visible'
        });
    </script>

    <script src="{{ asset('/plugins/isotope.pkgd.min.js') }}"></script>
    <script>
        // init Isotope
        $('.grid-portfolio').isotope({
            // options...
            itemSelector: '.grid-portfolio-item',
            masonry: {
                columnWidth: 0,
                gutter: 0
            }
        });
    </script>
@endsection
