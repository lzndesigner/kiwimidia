@extends('frontend.app')
@section('title', $result->meta_title)
@section('background', 'servicos.jpg')

@section('content')
<div class="w-full bg-white py-20 text-center mx-auto">

    <div class="container mx-auto my-10">
            <div class="w-full inline-block">
            <div class="p-6 flex flex-col-reverse lg:flex-row items-start justify-center space-x-4">
                <div class="mx-auto mt-10 lg:mt-0 lg:flex-shrink-0">
                   <img class="h-60 w-60 mx-auto rounded-full" src="{{ asset($result->image) }}"
                        alt="{{ $result->meta_title }}">
                </div>
                <div class="text-center lg:text-left">
                    {!! $result->content !!}
                </div>
            </div><!-- card -->
            </div><!-- card -->
    </div>
    <!-- flex -->
    </div><!-- Serviços -->

@endsection

@section('includeCSS')
@endsection
@section('includeJS')
@endsection
