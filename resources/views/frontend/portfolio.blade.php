@extends('frontend.app')
@section('title', 'Portfólio')
@section('background', 'portfolio.jpg')
@section('background-position', 'center')

@section('content')
    <!-- BEGIN: Portfolio -->
    <section class="w-full bg-white py-20 text-center mx-auto">

        <div class="container px-4 mx-auto my-10">
            <nav class="filters mb-10">
                <div class="button-group filter-button-group">
                    <button
                        class="rounded bg-black text-white border-2 border-black hover:bg-white hover:text-black px-4 py-1 mx-1 mb-2 uppercase text-sm"
                        data-filter="*">Todos</button>
                        @foreach ($getServices as $getService)
                            <button
                            class="rounded bg-white border-2 border-black hover:bg-black hover:text-white px-4 py-1 mx-1 mb-2 uppercase text-sm"
                            data-filter=".{{$getService->slug}}">{{$getService->meta_title}}</button>
                        @endforeach

                </div>
            </nav>
            <div class="grid-portfolio">
                @foreach ($results as $getPortfolio)
                    <div class="grid-item {{ str_replace(',', ' ', $getPortfolio->tags) }} inline-block w-full col-4 h-72 lg:h-96 mb-3 rounded relative {{ $getPortfolio->tags == 'videos' || $getPortfolio->tags == 'filmagem_aerea' || $getPortfolio->tags == 'videos,filmagem_aerea' || $getPortfolio->tags == 'filmagem_aerea,videos' ? 'openModalVideo' : 'openModalPortfolio' }}"
                        data-idportfolio="{{ $getPortfolio->id }}"
                        style="background-image: url('{{ asset($getPortfolio->image) }}');background-position: center;background-size:110%;">
                        <a href="javascript:;">
                            <div
                                class="lg:opacity-0 lg:hover:opacity-100 duration-300 absolute lg:rounded bottom-0 w-full lg:inset-0 z-10 py-4 lg:py-0 flex flew-wrap justify-center items-center bg-black bg-opacity-80">
                                <div class="w-4/5 mx-auto">
                                    <h2
                                        class="w-full font-montserrat text-white font-semibold text-lg lg:text-3xl tracking-normal uppercase mb-1 lg:mb-3">
                                        {{ $getPortfolio->meta_title }}</h2>
                                    <div
                                        class="w-full font-montserrat text-gray-100 font-light text-sm lg:text-sm overflow-ellipsis overflow-hidden">
                                        {{ $getPortfolio->meta_description }}
                                    </div>
                                </div><!-- mx-auto -->
                            </div>
                            <!--caption -->

                            @if ($getPortfolio->featured)
                                <div
                                    class="animate-bounce absolute px-2 py-1 rounded-r-lg w-auto top-2 left-2 text-yellow-300 text-base z-10">
                                    <i class="fa fa-star"></i>
                                </div>
                            @endif
                        </a>
                    </div><!-- grid-item -->

                @endforeach
            </div>
        </div><!-- container -->
    </section><!-- Portfólio -->


    <!-- Modal Portfolio -->
    <div class="bg-black bg-opacity-80 modal fade flex min-h-screen items-center justify-items-center invisible"
        id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="modalVideoLabel" aria-hidden="true">
        <div class="flex-grow w-3/4">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <button type="button" class="inline-block w-10 rounded bg-black text-gray-200 text-lg px-3 py-1 btnCloseModal"
                                data-dismiss="modal"><i class="fa fa-times"></i></button>
                    <form action="" class="form-horizontal" id="form-request">
                        <div class="modal-body" id="form-content">
                            <!-- conteudo -->
                            <!-- conteudo -->
                        </div><!-- modal-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('includeCSS')
@endsection
@section('includeJS')
    <script src="{{ asset('/plugins/isotope.pkgd.min.js') }}"></script>
    <script>
        // init Isotope
        var $grid = $('.grid-portfolio').isotope({
            // options
        });
        // filter items on button click
        $('.filter-button-group').on('click', 'button', function() {
            var filterValue = $(this).attr('data-filter');
            $('.filter-button-group button').removeClass('bg-black').removeClass('text-white').addClass('bg-white');
            $(this).removeClass('bg-white').addClass('bg-black').addClass('text-white');
            $grid.isotope({
                filter: filterValue
            });
        });

        $grid.isotope({
            // options...
            itemSelector: '.grid-item',
            masonry: {
                columnWidth: 0,
                gutter: 5
            }
        });
    </script>

<script src="{{ asset('/plugins/template.js') }}"></script>
@endsection
