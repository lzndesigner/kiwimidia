Nome: {{$email->name}} <br>
E-mail: {{$email->email}} <br>
@if(isset($email->telephone))
Telefone: {{$email->telephone}} <br>
@endif
@if(isset($email->subject))
Assunto: {{$email->subject}} <br>
@endif
Mensagem: {{$email->message}} <br> <br>
Data: {{\Carbon\Carbon::now()->format('d/m/Y H:i')}}