@extends('errors::illustrated-layout')

@section('title', __('Serviço indisponível'))
@section('code', '503')
@section('message', __('Serviço indisponível'))
