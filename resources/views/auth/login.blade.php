@extends('auth.app')

@section('content')
    <div class="box p-10 rounded30 box-shadowed">
        <div class="box-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group @error('email') has-error @enderror">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input type="email" class="form-control pl-15 plc-white" name="email" id="email"
                            value="{{ old('email') }}" placeholder="E-mail de Acesso" autocomplete="email" autofocus>
                    </div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group @error('password') has-error @enderror">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text "><i class="fa fa-lock"></i></span>
                        </div>
                        <input type="password" class="form-control pl-15 plc-white" name="password" id="password"
                            placeholder="Senha de Acesso" autocomplete="current-password">
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="checkbox ">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Lembrar-me</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    @if (Route::has('password.request'))
                        <div class="col-6">
                            <div class="fog-pwd text-right">
                                <a href="{{ route('password.request') }}" class=" hover-info">
                                    <i class="ion ion-locked"></i> Esqueceu sua senha?</a>
                            </div>
                        </div>
                    @endif
                    <!-- /.col -->
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-info mt-10">Acessar Painel</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div><!-- box-card -->
    </div><!-- box -->
@endsection
