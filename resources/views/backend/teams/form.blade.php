@extends('backend.app')

@section('title', isset($result) ? 'Editar Time - ' . $result->name : 'Novo Time')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('teams.index') }}">Times</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Time' : 'Novo Time' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <h4 class="box-title text-info"><i class="fa fa-user mr-1"></i> Informações</h4>
                                <div class="form-group">
                                    <label for="name" class="col-sm-12">Nome <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nome"
                                            value="{{ isset($result->name) ? $result->name : '' }}">
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="description" class="col-sm-12">Descrição:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="description" name="description"
                                            placeholder="Descrição"
                                            value="{{ isset($result->description) ? $result->description : '' }}">
                                        <span class="help-block">Máximo de 255 caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="status" class="col-sm-12">Status</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                            <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="image" class="col-sm-12">Imagem <span class="text-danger">*</span></label>
                                    <div class="row p-0 m-0">
                                        <div class="col-12 col-lg-12">
                                            <div class="input-group">
                                                <input id="image" class="form-control" type="text" name="image"
                                                    placeholder="Selecione uma Capa">
                                                <span class="input-group-btn">
                                                    <a id="lfm" data-input="image" data-preview="holder"
                                                        class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Selecionar Capa
                                                    </a>
                                                </span>
                                            </div>
                                            <span class="help-block">Apenas uma imagem, de preferência (800x800px)</span>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <div id="holder" class="border ">
                                                <img src="{{ isset($result->image) ? $result->image : '/sem_imagem.jpg' }}"
                                                    class="img-fluid">
                                            </div><!-- holder -->
                                        </div>
                                    </div><!-- ow -->
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-12 col-lg-6">
                                <h4 class="box-title text-info"><i class="fa fa-users mr-1"></i> Redes Sociais</h4>
                                <div class="form-group">
                                    <label>Facebook</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-facebook-square mr-2"></i> https://facebook.com/</span>
                                        </span>
                                        <input type="text" class="form-control" name="facebook" placeholder="seu_perfil"
                                        value="{{ isset($result->facebook) ? $result->facebook : '' }}">
                                    </div>                                            
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label>Instagram</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-instagram mr-2"></i> https://instagram.com/</span>
                                        </span>
                                        <input type="text" class="form-control" name="instagram" placeholder="seu_perfil"
                                        value="{{ isset($result->instagram) ? $result->instagram : '' }}">
                                    </div>                                            
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label>WhatsApp</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-whatsapp mr-2"></i> Telefone</span>
                                        </span>
                                        <input type="text" class="form-control maskPhone" name="whatsapp" placeholder="(00) 0000-9999"
                                        value="{{ isset($result->whatsapp) ? $result->whatsapp : '' }}">
                                    </div>                                            
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div><!-- row -->
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('teams.index') }}" class="btn btn-sm btn-rounded btn-info" data-toggle="tooltip"
                            data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- FileManager Laravel -->
    <script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    <script>
        $('#lfm').filemanager('image');
        var lfm = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };
    </script>

    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var method = 'POST';
            var url = "{{ route('teams.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('teams.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/teams/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('teams.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
        <script src="{{ asset('/plugins/jquery.mask.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                $('.maskPhone').mask("(99) 99999-9999");
                $('.maskPhone').on("blur", function() {
                var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
    
                if( last.length == 3 ) {
                    var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
                    var lastfour = move + last;
                    var first = $(this).val().substr( 0, 9 );
    
                    $(this).val( first + '-' + lastfour );
                }
                });;
    
            });
        </script>
@endsection
