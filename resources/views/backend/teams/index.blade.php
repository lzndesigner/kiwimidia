@extends('backend.app')

@section('title', 'Time')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="team">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <div class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div>
                            <h4 class="box-title">@yield('title')</h4>
                        </div>
                    </div>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->
                    <div class="table-responsive">
                        <table id="table-request" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="70px">Imagem</th>
                                    <th>Nome</th>
                                    <th>Criado</th>
                                    <th>Status</th>
                                    <th width="150px"><i class="fa fa-cogs mr-2"></i> Ações</th>
                                </tr>
                            </thead>
                            <tbody id="table-body">
                                @foreach ($teams as $team)
                                    <tr id="row_{{ $team->id }}">
                                        <td>{{ $team->id }}</td>
                                        <td><img src="{{ $team->image }}" alt="{{ $team->meta_title }}"
                                                class="img-thumb"></td>
                                        <td>{{ $team->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($team->created_at)->format('d/m/y H:i') }}</td>
                                        <td>{{ $team->status }}</td>
                                        <td>
                                            <a href="{{ route('teams.edit', $team->id) }}"
                                                class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top"
                                                title="Editar Time"><i class="fa fa-edit"></i></a>

                                            <a href="javascript:;" data-id="{{ $team->id }}"
                                                class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip"
                                                data-placement="top" title="Deletar Time?"><i
                                                    class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr class="m-4">
                    <div id="section-pagination"
                        class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div class="mt-2 mt-lg-0">
                            <select name="itensPage" id="itensPage" class="form-control" data-toggle="tooltip"
                                data-placement="top" title="Quantidade por Página">
                                <option value="5" {{ $itensPage == '5' ? 'selected' : '' }}>5</option>
                                @for ($i = 10; $i < 50; $i += 10)
                                    <option value="{{ $i }}" {{ $itensPage == $i ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                @endfor
                                <option value="50" {{ $itensPage == '50' ? 'selected' : '' }}>50</option>
                                <option value="100" {{ $itensPage == '100' ? 'selected' : '' }}>100</option>
                            </select>
                        </div>
                        {{ $teams->appends(request()->query())->links() }}
                    </div>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="{{ route('teams.create') }}" class="btn btn-rounded btn-success btn-mobile-float"
                            data-toggle="tooltip" data-placement="top" title="Novo Time"><i class="fa fa-plus"></i>
                            <span>Novo Time</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        // Change Quantity Pagination
        $('#itensPage').on('change', function() {
            var itensPage = $(this).val();
            var url = '/dashboard/teams?itensPage=' + itensPage; // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });

    </script>
    <script>
        // Button - Delete
        $('.btn-delete').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            let id = $(this).data('id');
            var url = `{{ url('dashboard/teams/${id}') }}`;

            Swal.fire({
                title: 'Deseja remover este registro?',
                text: "Você não poderá reverter isso!",
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        data: $('#form-table').serialize(),
                        success: function(data) {
                            // Loading team listagem
                            location.href = "{{ url('/dashboard/teams') }}";
                        },
                        error: function(xhr) {
                            if (xhr.status === 422) {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'warning',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            } else {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'error',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });

    </script>
@endsection
