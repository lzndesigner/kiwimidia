@extends('backend.app')

@section('title', 'Serviço - ' . $result->meta_title)

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <div class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div>
                            <h4 class="box-title">@yield('title')</h4>
                        </div>
                    </div>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    @if ($result->image)
                    <p><b>Capa:</b></p>
                        <div class="col-12 col-md-6 border p-3 mb-3">
                            <img src="{{ $result->image }}" alt="{{ $result->meta_title }}" class="img-fluid">
                        </div>
                    @endif

                    <p><b>Conteúdo:</b></p>
                    {!! $result->content !!}

                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="{{ route('services.edit', $result->id) }}"
                            class="btn btn-rounded btn-primary btn-mobile-float" data-toggle="tooltip" data-placement="top"
                            title="Editar Serviço"><i class="fa fa-edit"></i> <span>Editar Serviço</span></a>

                        <a href="{{ route('services.index') }}" class="btn btn-sm btn-rounded btn-info" data-toggle="tooltip"
                            data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection
