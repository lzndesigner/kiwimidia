@extends('backend.app')

@section('title', isset($result) ? 'Editar Serviço - ' . $result->name : 'Novo Serviço')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('services.index') }}">Serviços</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Serviço' : 'Novo Serviço' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="meta_title" class="col-sm-12">Título do Serviço <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_title" name="meta_title"
                                            placeholder="Título do Serviço"
                                            value="{{ isset($result->meta_title) ? $result->meta_title : '' }}">
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="slug" class="col-sm-12">URL Amigável <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ URL::to('/') }}/</span>
                                            </div>
                                            <input type="text" class="form-control" id="slug" name="slug"
                                                placeholder="titulo-da-pagina-sem-acentos-ou-espacos"
                                                value="{{ isset($result->slug) ? $result->slug : '' }}">
                                        </div>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_description" class="col-sm-12">Descrição do Serviço:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_description"
                                            name="meta_description" placeholder="Descrição do Serviço"
                                            value="{{ isset($result->meta_description) ? $result->meta_description : '' }}">
                                        <span class="help-block">Máximo de 255 caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_keywords" class="col-sm-12">Palavras-chave:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords"
                                            placeholder="Preencha com as Palavras-chave"
                                            value="{{ isset($result->meta_keywords) ? $result->meta_keywords : '' }}">
                                        <span class="help-block">Separe sempre por vírgula e o máximo de 255
                                            caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="status" class="col-sm-12">Status do Serviço</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                            <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="image" class="col-sm-12">Capa do Serviço <span class="text-danger">*</span></label>
                                    <div class="row p-0 m-0">
                                        <div class="col-12 col-lg-9">
                                            <div class="input-group">
                                                <input id="image" class="form-control" type="text" name="image"
                                                    placeholder="Selecione uma Capa">
                                                <span class="input-group-btn">
                                                    <a id="lfm" data-input="image" data-preview="holder"
                                                        class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Selecionar Capa
                                                    </a>
                                                </span>
                                            </div>
                                            <span class="help-block">Apenas uma imagem, de preferência (800x800px)</span>
                                        </div>
                                        <div class="col-12 col-lg-3">
                                            <div id="holder" class="border ">
                                                <img src="{{ isset($result->image) ? $result->image : '/sem_imagem.jpg' }}"
                                                    class="img-fluid">
                                            </div><!-- holder -->
                                        </div>
                                    </div><!-- ow -->
                                </div><!-- form-group -->




                            </div><!-- col -->
                        </div><!-- row -->
                        <div class="row">
                            <div class="col-12">
                                <hr>
                                <div class="form-group">
                                    <label for="content" class="col-sm-12">Conteúdo do Serviço <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="15" id="content" name="content"
                                            placeholder="Preencha com o conteúdo do Serviço...">{{ isset($result->content) ? $result->content : '' }}</textarea>
                                    </div>
                                </div><!-- form-group -->

                            </div>
                        </div>
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('services.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- FileManager Laravel -->
    <script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    <script>
        $('#lfm').filemanager('image');
        var lfm = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };

    </script>

    <!-- CK Editor -->
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/dashboard/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/dashboard/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/dashboard/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/dashboard/laravel-filemanager/upload?type=Files&_token='
        };

        CKEDITOR.replace('content', options);

    </script>


    <!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
    <script type="text/javascript" src="{{ asset('/plugins/jquery.stringToSlug.min.js') }}"></script>
    <script>
        $(function() {
            $('input[name="meta_title"]').stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: 'input[name="slug"]',
                space: '-',
                replace: '/\s?\([^\)]*\)/gi',
                AND: 'e'
            });
        });

    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            var method = 'POST';
            var url = "{{ route('services.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('services.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/services/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('services.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
