@extends('backend.app')

@section('title', isset($result) ? 'Editar Cliente - ' . $result->name : 'Novo Cliente')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('customers.index') }}">Clientes Admin</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Cliente' : 'Novo Cliente' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->
                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="form-group">
                                    <label for="meta_title" class="col-sm-12">Nome do Cliente <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_title" name="meta_title"
                                            placeholder="Nome do Cliente"
                                            value="{{ isset($result->meta_title) ? $result->meta_title : '' }}">
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="web_link" class="col-sm-12">Link de Site:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="web_link" name="web_link"
                                            placeholder="http://linkdocliente.com.br"
                                            value="{{ isset($result->web_link) ? $result->web_link : '' }}">
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="status" class="col-sm-12">Status do Cliente</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                            <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->
                                
                                <div class="form-group">
                                    <label for="sort_order" class="col-sm-12">Posição de Exibição:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="sort_order" name="sort_order"
                                            placeholder="Posição"
                                            value="{{ isset($result->sort_order) ? $result->sort_order : '' }}">
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-12 col-lg-7">
                                <div class="form-group">
                                    <label for="image">Imagem do Cliente <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input id="image" class="form-control" type="text" name="image"
                                            placeholder="Selecione uma imagem no botão ao lado">
                                        <span class="input-group-btn">
                                            <a id="lfm" data-input="image" data-preview="holder" class="btn btn-primary">
                                                <i class="fa fa-picture-o"></i> Selecionar Imagem
                                            </a>
                                        </span>
                                    </div>
                                    <span class="help-block">Apenas uma imagem</span>
                                    <div id="holder" class="border mt-2 ml-4 p-3 col-4">
                                        <img src="{{ isset($result->image) ? $result->image : '/sem_imagem.jpg' }}"
                                            class="img-fluid">
                                    </div><!-- holder -->
                                </div><!-- form-group -->
                            </div>
                        </div><!-- row -->
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('customers.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- FileManager Laravel -->
    <script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    <script>
        $('#lfm').filemanager('image');
        var lfm = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };

    </script>

    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            var method = 'POST';
            var url = "{{ route('customers.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('customers.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/customers/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('customers.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
