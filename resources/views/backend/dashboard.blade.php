@extends('backend.app')
@section('title', 'Dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboad') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">
                <div class="box-body">
                    <p>Welcome to base project Laravel 8</p>
                    <p>Base inicial para projeto que requer dashboard!</p>
                </div>
            </div>
        </div>
    </div>
@endsection
