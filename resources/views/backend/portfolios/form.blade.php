@extends('backend.app')

@section('title', isset($result) ? 'Editar Portfólio - ' . $result->name : 'Novo Portfólio')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('portfolios.index') }}">Portfólios</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Portfólio' : 'Novo Portfólio' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="meta_title" class="col-sm-12">Título do Portfólio <span
                                            class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_title" name="meta_title"
                                            placeholder="Título do Portfólio"
                                            value="{{ isset($result->meta_title) ? $result->meta_title : '' }}">
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="slug" class="col-sm-12">URL Amigável <span
                                            class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ URL::to('/') }}/</span>
                                            </div>
                                            <input type="text" class="form-control" id="slug" name="slug"
                                                placeholder="titulo-da-pagina-sem-acentos-ou-espacos"
                                                value="{{ isset($result->slug) ? $result->slug : '' }}">
                                        </div>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_description" class="col-sm-12">Descrição do Portfólio:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_description"
                                            name="meta_description" placeholder="Descrição do Portfólio"
                                            value="{{ isset($result->meta_description) ? $result->meta_description : '' }}">
                                        <span class="help-block">Máximo de 255 caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_keywords" class="col-sm-12">Palavras-chave:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords"
                                            placeholder="Preencha com as Palavras-chave"
                                            value="{{ isset($result->meta_keywords) ? $result->meta_keywords : '' }}">
                                        <span class="help-block">Separe sempre por vírgula e o máximo de 255
                                            caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="tags" class="col-sm-12">Tags Filtros:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="tags" name="tags"
                                            placeholder="Tags para Filtros" data-role="tagsinput"
                                            value="{{ isset($result->tags) ? $result->tags : '' }}">
                                    </div>
                                    <span class="help-block pl-2">Digite cada TAG e aperte o Enter</span>

                                    <div class="pl-3">
                                        @foreach ($getServices as $getService)
                                        <a href="#" class="badge badge-sm badge-primary p-2 mr-2 mb-3 addItem"
                                            data-name="{{$getService->slug}}">{{$getService->meta_title}}</a>
                                        @endforeach                                            
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="featured" class="col-sm-12">Em Destaque? <span
                                                    class="text-danger">*</span></label>
                                            <div class="flex flex-wrap">
                                                <div class="radio pl-10">
                                                    <input name="featured" type="radio" id="yes" value="1" @if (isset($result->featured) && $result->featured == '1') checked @endif>
                                                    <label for="yes" class="mr-10">Sim</label>
        
                                                    <input name="featured" type="radio" id="no" value="0" @if (isset($result->featured) && $result->featured == '0') checked @endif>
                                                    <label for="no">Não</label>
                                                </div>
                                            </div>
                                        </div><!-- form -->
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sort_order" class="col-sm-12">Posição de Exibição:</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control" id="sort_order" name="sort_order"
                                                    placeholder="Posição"
                                                    value="{{ isset($result->sort_order) ? $result->sort_order : '' }}">
                                            </div>
                                        </div><!-- form-group -->
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-sm-12">Status do Portfólio</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                            <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="image" class="col-sm-12">Capa do Portfólio <span
                                            class="text-danger">*</span></label>
                                    <div class="row p-0 m-0">
                                        <div class="col-12 col-lg-9">
                                            <div class="input-group">
                                                <input id="image" class="form-control" type="text" name="image"
                                                    placeholder="Selecione uma Capa">
                                                <span class="input-group-btn">
                                                    <a id="lfm" data-input="image" data-preview="holder"
                                                        class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Selecionar Capa
                                                    </a>
                                                </span>
                                            </div>
                                            <span class="help-block">Apenas uma imagem, de preferência (800x800px)</span>
                                        </div>
                                        <div class="col-12 col-lg-3">
                                            <div id="holder" class="border ">
                                                <img src="{{ isset($result->image) ? $result->image : '/sem_imagem.jpg' }}"
                                                    class="img-fluid">
                                            </div><!-- holder -->
                                        </div>
                                    </div><!-- ow -->
                                </div><!-- form-group -->


                                <div class="form-group">
                                    <label for="imagens" class="col-sm-12">Miniaturas do Portfólio</label>
                                    <div class="row p-0 m-0">
                                        <div class="col-12 col-lg-12">
                                            <div class="input-group">
                                                <input id="imagens" class="form-control" type="text" name="imagens[]"
                                                    multiple placeholder="Selecione uma Miniatura">
                                                <span class="input-group-btn">
                                                    <a id="lfms" data-input="imagens" data-preview="holders"
                                                        class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Selecionar Miniatura
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-12 mt-3">
                                            <div class="gallery-midias border">
                                                @if (isset($miniaturas))
                                                    @foreach ($miniaturas as $miniatura)
                                                        <span>
                                                            <img src="{{ isset($miniatura['image']) ? $miniatura['image'] : '/sem_imagem.jpg' }}"
                                                                class="img-thumb m-1">
                                                            <a href="#" class="btn-delete-image"
                                                                data-portfolio-id="{{ $result->id }}"
                                                                data-image-id="{{ $miniatura['id'] }}"><i
                                                                    class="fa fa-times"></i></a>
                                                        </span>
                                                    @endforeach
                                                @endif
                                            </div>
                                            <div id="holders" class="border p-2 m-2">
                                            </div><!-- holder -->
                                        </div>
                                    </div><!-- ow -->
                                </div><!-- form-group -->




                            </div><!-- col -->
                        </div><!-- row -->
                        <div class="row">
                            <div class="col-12">
                                <hr>
                                <div class="form-group">
                                    <label for="content" class="col-sm-12">Conteúdo do Portfólio <span
                                            class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="15" id="content" name="content"
                                            placeholder="Preencha com o conteúdo do Portfólio...">{{ isset($result->content) ? $result->content : '' }}</textarea>
                                    </div>
                                </div><!-- form-group -->

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <hr>
                                <div class="form-group">
                                    <label for="content" class="col-sm-12">Vídeos do Portfólio: <button
                                            class="btn btn-xs btn-secondary ml-3 hide" onclick="addOptionValue();"
                                            type="button"><i class="fa fa-plus"></i>
                                            novo</button></label>
                                    <div class="col-sm-12" id="content-videos">

                                        @if (isset($result->videos) && $result->videos != null)
                                            @foreach (json_decode($result->videos) as $videos)
                                                <div class="input-group mb-2">
                                                    <input type="text" class="form-control" name="videos[]"
                                                        value="{{ $videos }}">
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group mb-2">
                                                <input type="text" class="form-control" id="videos" name="videos[]"
                                                    value="">
                                            </div>
                                        @endif
                                        <span class="help-block">- Apenas um vídeo por portfólio</span>
                                    </div>
                                </div><!-- form-group -->

                            </div>
                        </div>
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('portfolios.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- videos -->
    <script>
        function removeInputEmpty(){
            var inputEmpty = $("#content-videos input").filter(function() {
            return !this.value;
            }).get();

            if (inputEmpty.length) {
            $(inputEmpty).remove();
            }
        }
        function addOptionValue() {
            html = '<div class="input-group mb-2">';
            html += '<input type="text" class="form-control" name="videos[]" value="">';
            html += '</div>';

            $('#content-videos').append(html);
        }
    </script>

    <!-- tags -->
    <script src="/backend/assets/vendor_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>

    <!-- FileManager Laravel -->
    <script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    <script>
        $('#lfm').filemanager('image');
        var lfm = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };

        // Miniaturas
        $('#lfms').filemanager('imagens');
        var lfms = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };
    </script>
    <!-- CK Editor -->
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/dashboard/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/dashboard/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/dashboard/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/dashboard/laravel-filemanager/upload?type=Files&_token='
        };

        CKEDITOR.replace('content', options);
    </script>
    <!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
    <script type="text/javascript" src="{{ asset('/plugins/jquery.stringToSlug.min.js') }}"></script>
    <script>
        $(function() {
            $('input[name="meta_title"]').stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: 'input[name="slug"]',
                space: '-',
                replace: '/\s?\([^\)]*\)/gi',
                AND: 'e'
            });
        });
    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            removeInputEmpty();

            var method = 'POST';
            var url = "{{ route('portfolios.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('portfolios.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            removeInputEmpty();

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/portfolios/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('portfolios.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
    <script>
        // Button - Delete Imagem Miniatura
        $('.btn-delete-image').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            let portfolio_id = $(this).data('portfolio-id');
            let image_id = $(this).data('image-id');
            var url = `{{ url('dashboard/portfolio-image/${portfolio_id}/${image_id}') }}`;

            Swal.fire({
                title: 'Deseja remover esta imagem?',
                text: "Você não poderá reverter isso!",
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        data: $('#form-table').serialize(),
                        success: function(data) {
                            // Loading portfolio listagem
                            location.reload();
                        },
                        error: function(xhr) {
                            if (xhr.status === 422) {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'warning',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            } else {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'error',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>
    <script>
        $('.addItem').click(function(e) {
            e.preventDefault();
            var nameItem = $(this).data('name');
            var valueOld = $('.bootstrap-tagsinput input').val();
            $('.bootstrap-tagsinput input').val(valueOld + nameItem + ', ').click();
        });
    </script>
@endsection
