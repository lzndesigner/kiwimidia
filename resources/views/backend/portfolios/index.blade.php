@extends('backend.app')

@section('title', 'Portfólio')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="portfolio">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <div class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div>
                            <h4 class="box-title">@yield('title')</h4>
                        </div>
                    </div>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->
                    <div class="table-responsive">
                        <table id="table-request" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="70px">Imagem</th>
                                    <th>Posição</th>
                                    <th>Título</th>
                                    <th>Criado</th>
                                    <th>Status</th>
                                    <th width="150px"><i class="fa fa-cogs mr-2"></i> Ações</th>
                                </tr>
                            </thead>
                            <tbody id="table-body">
                                @foreach ($portfolios as $portfolio)
                                    <tr id="row_{{ $portfolio->id }}">
                                        <td>{{ $portfolio->id }}</td>
                                        <td><img src="{{ $portfolio->image }}" alt="{{ $portfolio->meta_title }}"
                                                class="img-thumb"></td>
                                        <td>
                                            <form name="form_position" method="post">
                                                <select name="change_position" id="change_position{{ $portfolio->id }}"
                                                    onchange="changeposition({{ $portfolio->id }})"
                                                    data-idportfolio="{{ $portfolio->id }}">
                                                    @if (!$portfolio->sort_order)
                                                        <option value="">-</option>
                                                    @endif

                                                    @for ($i = 1; $i < 100; $i++)
                                                        <option value="{{ $i }}"
                                                            {{ $i == $portfolio->sort_order ? 'selected' : '' }}>
                                                            {{ $i }}</option>
                                                    @endfor

                                                </select>
                                            </form>
                                        </td>
                                        <td>{{ $portfolio->meta_title }} {!! $portfolio->featured ? '<br/><span class="badge badge-sm badge-primary">Destaque</span>' : '' !!}</span></td>
                                        <td>{{ \Carbon\Carbon::parse($portfolio->created_at)->format('d/m/y H:i') }}</td>
                                        <td>{{ $portfolio->status }}</td>
                                        <td>
                                            <a href="{{ route('portfolios.show', $portfolio->slug) }}"
                                                class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top"
                                                title="Ver Portfólio"><i class="fa fa-eye"></i></a>

                                            <a href="{{ route('portfolios.edit', $portfolio->id) }}"
                                                class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top"
                                                title="Editar Portfólio"><i class="fa fa-edit"></i></a>

                                            <a href="javascript:;" data-id="{{ $portfolio->id }}"
                                                class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip"
                                                data-placement="top" title="Deletar Portfólio?"><i
                                                    class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <hr class="m-4">
                    <div id="section-pagination"
                        class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div class="mt-2 mt-lg-0">
                            <select name="itensPage" id="itensPage" class="form-control" data-toggle="tooltip"
                                data-placement="top" title="Quantidade por Página">
                                <option value="5" {{ $itensPage == '5' ? 'selected' : '' }}>5</option>
                                @for ($i = 10; $i < 50; $i += 10)
                                    <option value="{{ $i }}" {{ $itensPage == $i ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                @endfor
                                <option value="50" {{ $itensPage == '50' ? 'selected' : '' }}>50</option>
                                <option value="100" {{ $itensPage == '100' ? 'selected' : '' }}>100</option>
                            </select>
                        </div>
                        {{ $portfolios->appends(request()->query())->links() }}
                    </div>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="{{ route('portfolios.create') }}" class="btn btn-rounded btn-success btn-mobile-float"
                            data-toggle="tooltip" data-placement="top" title="Novo Portfólio"><i class="fa fa-plus"></i>
                            <span>Novo Portfólio</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        // Change Quantity Pagination
        $('#itensPage').on('change', function() {
            var itensPage = $(this).val();
            var url = '/dashboard/portfolios?itensPage=' + itensPage; // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
    </script>
    <script>
        // Button Search
        $(document).on('click', '#btn-search', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            var method = 'POST';
            var url = "{{ url('dashboard/portfolios/search') }}";
            var data = $('#form-search').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    $('.page-title, .box-title').html('Resultado da Busca')
                    $('#table-body').html('');
                    $('#section-pagination').html('');
                    $('#table-body').html(data);
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: xhr.responseJSON,
                            icon: 'info',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>
    <script>
        // Button - Delete
        $('.btn-delete').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            let id = $(this).data('id');
            var url = `{{ url('dashboard/portfolios/${id}') }}`;

            Swal.fire({
                title: 'Deseja remover este registro?',
                text: "Você não poderá reverter isso!",
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        data: $('#form-table').serialize(),
                        success: function(data) {
                            // Loading portfolio listagem
                            // location.href = "{{ url('/dashboard/portfolios') }}";
                            location.reload();
                        },
                        error: function(xhr) {
                            if (xhr.status === 422) {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'warning',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            } else {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'error',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>
    <script>
        function changeposition(id_portfolio) {
            var new_order = $('#change_position' + id_portfolio).val();
            var url = `{{ url('dashboard/portfolios/change/${id_portfolio}/${new_order}') }}`;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $.ajax({
                url: url,
                method: 'POST',
                data: $('#change_position' + id_portfolio).serialize(),
                success: function(data) {
                    // Loading portfolio listagem
                    // location.href = "{{ url('/dashboard/portfolios') }}";
                    location.reload();
                }
            });
        }
    </script>
@endsection
