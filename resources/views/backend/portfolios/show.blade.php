@extends('backend.app')

@section('title', 'Portfólio - ' . $result->meta_title)

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <div class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div>
                            <h4 class="box-title">@yield('title')</h4>
                        </div>
                    </div>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    @if ($result->image)
                        <p><b>Capa:</b></p>
                        <div class="col-12 col-md-6 border p-3 mb-3">
                            <img src="{{ $result->image }}" alt="{{ $result->meta_title }}" class="img-fluid">
                        </div>
                    @endif

                    <p><b>Conteúdo:</b></p>
                    {!! $result->content !!}


                    @if (isset($miniaturas))
                        <hr>
                        <p><b>Miniaturas</b></p>
                        <div class="row">
                            @if (isset($miniaturas))
                                @foreach ($miniaturas as $miniatura)
                                    <div class="col-12 col-lg-3 gallery-midias">
                                        <span>
                                            <a href="{{ isset($miniatura['image']) ? $miniatura['image'] : '/sem_imagem.jpg' }}" data-fancybox="gallery">
                                                <img src="{{ isset($miniatura['image']) ? $miniatura['image'] : '/sem_imagem.jpg' }}"
                                                    class="img-fluid border p-2 m-1">
                                            </a>
                                            
                                            <a href="#" class="btn-delete-image" data-portfolio-id="{{ $result->id }}"
                                                data-image-id="{{ $miniatura['id'] }}"><i class="fa fa-times"></i></a>
                                        </span>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="{{ route('portfolios.edit', $result->id) }}"
                            class="btn btn-rounded btn-primary btn-mobile-float" data-toggle="tooltip" data-placement="top"
                            title="Editar Portfólio"><i class="fa fa-edit"></i> <span>Editar Portfólio</span></a>

                        <a href="{{ route('portfolios.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection


@section('includeCSS')
    <!-- FancyBox -->
    <link rel="stylesheet" href="{{ asset('/plugins/fancybox/jquery.fancybox.min.css') }}" />

    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- FancyBox -->
    <script src="{{ asset('/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
    <script>
        $('[data-fancybox="gallery"]').fancybox({
            selector: '.imglist a:visible'
        });

    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        // Button - Delete Imagem Miniatura
        $('.btn-delete-image').click(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            let portfolio_id = $(this).data('portfolio-id');
            let image_id = $(this).data('image-id');
            var url = `{{ url('dashboard/portfolio-image/${portfolio_id}/${image_id}') }}`;

            Swal.fire({
                title: 'Deseja remover esta imagem?',
                text: "Você não poderá reverter isso!",
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        method: 'DELETE',
                        data: $('#form-table').serialize(),
                        success: function(data) {
                            // Loading portfolio listagem
                            location.reload();
                        },
                        error: function(xhr) {
                            if (xhr.status === 422) {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'warning',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            } else {
                                Swal.fire({
                                    text: xhr.responseJSON,
                                    icon: 'error',
                                    showClass: {
                                        popup: 'animate_animated animate_wobble'
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });

    </script>
@endsection
