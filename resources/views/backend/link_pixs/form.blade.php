@extends('backend.app')

@section('title', isset($result) ? 'Editar Link - ' . $result->name : 'Novo Link')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
    </li>
    <li class="breadcrumb-item" aria-current="page"><a href="{{ route('link_pixs.index') }}">Links Admin</a></li>
    <li class="breadcrumb-item active" aria-current="page">
        {{ isset($result) ? 'Editar Link' : 'Novo Link' }}
    </li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-12">
        <div class="box">

            <div class="box-header with-border">
                <h4 class="box-title">@yield('title')</h4>
            </div><!-- box-header -->

            <div class="box-body">
                <!-- conteudo -->
                <form id="form-request" method="POST" class="form-horizontal">
                    <input type="hidden" name="slug" id="slug" value="">
                    <div class="row">
                        <div class="col-12 col-lg-5">
                            <div class="form-group">
                                <label for="name" class="col-sm-12">Nome <span class="text-danger">*</span>:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nome" value="{{ isset($result->name) ? $result->name : '' }}">
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label for="description" class="col-sm-12">Descrição <span class="text-danger">*</span>:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="description" name="description" placeholder="Nome" value="{{ isset($result->description) ? $result->description : '' }}">
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label for="status" class="col-sm-12">Status do Link</label>
                                <div class="col-sm-12">
                                    <select name="status" id="status" class="form-control">
                                        <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                        <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                    </select>
                                </div>
                            </div><!-- form-group -->

                        </div><!-- col -->
                    </div><!-- row -->
                </form>
                <!-- conteudo -->
            </div><!-- box-body -->

            <div class="box-footer flexbox">
                <div class="text-left flex-grow">
                    <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}" data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i class="fa fa-check"></i> Salvar</a>
                    <a href="{{ route('link_pixs.index') }}" class="btn btn-sm btn-rounded btn-info" data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                        <span>Voltar</span></a>
                </div>
            </div><!-- box-footer -->

        </div><!-- box -->
    </div><!-- cols -->
</div><!-- row -->
@endsection

@section('includeCSS')
<!-- Include SweetAlert -->
<link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="{{ asset('/plugins/jquery.stringToSlug.min.js') }}"></script>
<script>
    $(function() {
        $('input[name="name"]').stringToSlug({
            setEvents: 'keyup keydown blur',
            getPut: 'input[name="slug"]',
            space: '-',
            replace: '/\s?\([^\)]*\)/gi',
            AND: 'e'
        });
    });
</script>

<!-- Include SweetAlert -->
<script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
<script>
    $(document).on('click', '#btn-salvar', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });
        var method = 'POST';
        var url = "{{ route('link_pixs.store') }}";
        var data = $('#form-request').serialize();

        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                Swal.fire({
                    text: data,
                    icon: 'success',
                    showClass: {
                        popup: 'animate_animated animate_backInUp'
                    },
                    onClose: () => {
                        // Redirect Page Listagem
                        location.href = "{{ route('link_pixs.index') }}";
                    }
                });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }
            }
        });
    });
</script>
<script>
    $(document).on('click', '#btn-edit', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        var data = $('#form-request').serialize();

        let id = $(this).data('id');
        var method = 'PUT';
        var url = `{{ url('dashboard/link_pixs/${id}') }}`;

        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                Swal.fire({
                    text: data,
                    icon: 'success',
                    showClass: {
                        popup: 'animate_animated animate_backInUp'
                    },
                    onClose: () => {
                        // Redirect Page Listagem
                        location.href = "{{ route('link_pixs.index') }}";
                    }
                });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }
            }
        });
    });
</script>
@endsection