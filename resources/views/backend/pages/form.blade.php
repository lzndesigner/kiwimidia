@extends('backend.app')

@section('title', isset($result) ? 'Editar Página - ' . $result->name : 'Nova Página')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('pages.index') }}">Páginas de Infor.</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Página' : 'Nova Página' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="form-group">
                                    <label for="meta_title" class="col-sm-12">Título da Página <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_title" name="meta_title"
                                            placeholder="Título da Página"
                                            value="{{ isset($result->meta_title) ? $result->meta_title : '' }}">
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_description" class="col-sm-12">Descrição da Página:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_description"
                                            name="meta_description" placeholder="Descrição da Página"
                                            value="{{ isset($result->meta_description) ? $result->meta_description : '' }}">
                                        <span class="help-block">Máximo de 255 caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label for="meta_keywords" class="col-sm-12">Palavras-chave:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords"
                                            placeholder="Preencha com as Palavras-chave"
                                            value="{{ isset($result->meta_keywords) ? $result->meta_keywords : '' }}">
                                        <span class="help-block">Separe sempre por vírgula e o máximo de 255
                                            caracteres.</span>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="col-12 col-lg-7">
                                <div class="form-group">
                                    <label for="status" class="col-sm-12">Status da Página</label>
                                    <div class="col-sm-12">
                                        <select name="status" id="status" class="form-control">
                                            <option value="habilitado" @if (isset($result->status) && $result->status == 'habilitado') selected @endif>habilitado</option>
                                            <option value="desabilitado" @if (isset($result->status) && $result->status == 'desabilitado') selected @endif>desabilitado</option>
                                        </select>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label for="slug" class="col-sm-12">URL Amigável <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ URL::to('/') }}/</span>
                                            </div>
                                            <input type="text" class="form-control" id="slug" name="slug"
                                                placeholder="titulo-da-pagina-sem-acentos-ou-espacos"
                                                value="{{ isset($result->slug) ? $result->slug : '' }}">
                                        </div>
                                    </div>
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div><!-- row -->
                        <div class="row">
                            <div class="col-12">
                                <hr>
                                <div class="form-group">
                                    <label for="content" class="col-sm-12">Conteúdo da Página <span class="text-danger">*</span>:</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control summernote" rows="15" id="content" name="content"
                                            placeholder="Preencha com o conteúdo da Página...">{{ isset($result->content) ? $result->content : '' }}</textarea>
                                    </div>
                                </div><!-- form-group -->

                            </div>
                        </div>
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('pages.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/dashboard/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/dashboard/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/dashboard/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/dashboard/laravel-filemanager/upload?type=Files&_token='
        };

        CKEDITOR.replace('content', options);
    </script>


    <!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
    <script type="text/javascript" src="{{ asset('/plugins/jquery.stringToSlug.min.js') }}"></script>
    <script>
        $(function() {
            $('input[name="meta_title"]').stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: 'input[name="slug"]',
                space: '-',
                replace: '/\s?\([^\)]*\)/gi',
                AND: 'e'
            });
        });

    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            for ( instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }

            var method = 'POST';
            var url = "{{ route('pages.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('pages.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            
            for ( instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/pages/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('pages.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
