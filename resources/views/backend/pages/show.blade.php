@extends('backend.app')

@section('title', 'Paginas - '.$result->meta_title)

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <div class="d-flex flex-column flex-lg-row justify-content-between align-items-center">
                        <div>
                            <h4 class="box-title">@yield('title')</h4>
                        </div>
                    </div>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                        {!! $result->content !!}
                                            
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="{{ route('pages.edit', $result->id) }}" class="btn btn-rounded btn-primary btn-mobile-float"
                            data-toggle="tooltip" data-placement="top" title="Editar Página"><i
                                class="fa fa-edit"></i> <span>Editar Página</span></a>

                        <a href="{{ route('pages.index') }}" class="btn btn-sm btn-rounded btn-info"
                            data-toggle="tooltip" data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i>
                            <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection