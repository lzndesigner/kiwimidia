@extends('backend.app')

@section('title', 'Contador')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-body">
                    <!-- conteudo -->
                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <div class="form-group">
                                    <label>Tempo de Execução</label>
                                    <input type="text" class="form-control" name="time"
                                        placeholder="Tempo de Execução em ms (4850)"
                                        value="{{ isset($counter->time) ? $counter->time : '' }}">
                                </div>
                            </div><!-- col 3 -->                            
                        </div><!-- row -->
                        <hr class="my-3">
                        <div class="row">
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <h4 class="box-title text-info"><i class="fa fa-desktop mr-1"></i> Contador #1</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Título #1</label>
                                            <input type="text" class="form-control" name="title_1"
                                                placeholder="Título Box #1"
                                                value="{{ isset($counter->title_1) ? $counter->title_1 : '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Número #1</label>
                                            <input type="text" class="form-control" name="number_1"
                                                placeholder="Número Box #1"
                                                value="{{ isset($counter->number_1) ? $counter->number_1 : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col-3-->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <h4 class="box-title text-primary"><i class="fa fa-desktop mr-1"></i> Contador #2</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Título #2</label>
                                            <input type="text" class="form-control" name="title_2"
                                                placeholder="Título Box #2"
                                                value="{{ isset($counter->title_2) ? $counter->title_2 : '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Número #2</label>
                                            <input type="text" class="form-control" name="number_2"
                                                placeholder="Número Box #2"
                                                value="{{ isset($counter->number_2) ? $counter->number_2 : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col-3-->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <h4 class="box-title text-danger"><i class="fa fa-desktop mr-1"></i> Contador #3</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Título #3</label>
                                            <input type="text" class="form-control" name="title_3"
                                                placeholder="Título Box #3"
                                                value="{{ isset($counter->title_3) ? $counter->title_3 : '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Número #3</label>
                                            <input type="text" class="form-control" name="number_3"
                                                placeholder="Número Box #3"
                                                value="{{ isset($counter->number_3) ? $counter->number_3 : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col-3-->
                            <div class="col-sm-12 col-md-3 col-lg-3">
                                <h4 class="box-title text-warning"><i class="fa fa-desktop mr-1"></i> Contador #4</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Título #4</label>
                                            <input type="text" class="form-control" name="title_4"
                                                placeholder="Título Box #4"
                                                value="{{ isset($counter->title_4) ? $counter->title_4 : '' }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Número #4</label>
                                            <input type="text" class="form-control" name="number_4"
                                                placeholder="Número Box #4"
                                                value="{{ isset($counter->number_4) ? $counter->number_4 : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col-3-->

                        </div><!-- row -->
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" id="btn-save" class="btn btn-rounded btn-success btn-mobile-float"
                            data-toggle="tooltip" data-placement="top" title="Salvar Alterações"><i class="fa fa-check"></i>
                            <span>Salvar Alterações</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-save', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var data = $('#form-request').serialize();

            let id = 1;
            var method = 'PUT';
            var url = `{{ url('dashboard/counter/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('counter.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });
    </script>

    <script src="{{ asset('/plugins/jquery.mask.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.maskPhone').mask("(99) 99999-9999");
            $('.maskPhone').on("blur", function() {
                var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                if (last.length == 3) {
                    var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
                    var lastfour = move + last;
                    var first = $(this).val().substr(0, 9);

                    $(this).val(first + '-' + lastfour);
                }
            });;

        });
    </script>
@endsection
