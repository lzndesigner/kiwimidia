<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\{
    HomepageController,
};
use App\Http\Controllers\Backend\{
    DashboardController,
    UserController,
    ConfigsController,
    PagesController,
    CustomersController,
    ServicesController,
    PortfoliosController,
    TeamsController,
    LinkPixsController,
    CountersController,
};

// *****************************************************

// Routes Site - Frontend
Route::get('/', [HomepageController::class, 'index']);
Route::get('/sobre', [HomepageController::class, 'about']);
Route::get('/servicos', [HomepageController::class, 'services']);
Route::get('/servico/{slugService}', [HomepageController::class, 'showService']);
Route::get('/portfolio', [HomepageController::class, 'portfolio']);
Route::get('/portfolio/{slugPortfolio}', [HomepageController::class, 'showPortfolio']);
Route::get('/portfolio/video/{id}', [HomepageController::class, 'portfolioVideos']);
Route::get('/portfolio/photos/{id}', [HomepageController::class, 'portfolioPhotos']);

Route::get('/clientes', [HomepageController::class, 'clientes']);
Route::get('/contato', [HomepageController::class, 'contato']);
Route::post('/contato-sendmail', [HomepageController::class, 'sendMail']);

Route::get('/redirect/{slug}', [HomepageController::class, 'showLinkPix']);

// *****************************************************

// Routes Admin - Middleware Auth
Auth::routes();

Route::prefix('dashboard')->middleware('auth')->group(function () {
    // Home
    Route::resource('/', DashboardController::class);

    // Resources
    Route::group(['prefix' => 'laravel-filemanager'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });

    // Usuários Admin
    Route::resource('users', UserController::class);
    Route::get('users/showsearch', [UserController::class, 'showsearch'])->name('users.showsearch');
    Route::post('users/search', [UserController::class, 'search'])->name('users.search');
    
    // Configurações Geral
    Route::get('configs', [ConfigsController::class, 'index'])->name('configs');
    Route::put('configs/{id}', [ConfigsController::class, 'update'])->name('configs.update');

    // Counter
    Route::resource('counter', CountersController::class);
    Route::put('counter/{id}', [CountersController::class, 'update'])->name('counter.update');
    
    // Paginas de Informações
    Route::resource('pages', PagesController::class);
    Route::get('pages/{slugPage}', [PagesController::class, 'show']);
    
    // Clientes
    Route::resource('customers', CustomersController::class);
    Route::post('customers/change/{id}/{new_order}', [CustomersController::class, 'change_position']);
    
    Route::resource('link_pixs', LinkPixsController::class);
    
    // Serviços
    Route::resource('services', ServicesController::class);
    Route::get('services/{slugService}', [ServicesController::class, 'show']);
    
    // Porfolio
    Route::resource('portfolios', PortfoliosController::class);
    Route::get('portfolios/{slugPortfolio}', [PortfoliosController::class, 'show']);
    Route::delete('portfolio-image/{portfolioID}/{imageID}', [PortfoliosController::class, 'destroyImage']);
    Route::post('portfolios/change/{id}/{new_order}', [PortfoliosController::class, 'change_position']);

    // Time
    Route::resource('teams', TeamsController::class);

   
});
