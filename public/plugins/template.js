// Close Modal
$('.btnCloseModal').click(function (e) {
    $("#form-content").html('');
    $("body").removeClass('modal-open');
    $("#modalVideo").addClass('invisible');
    $("#form-content").removeClass('iframe-responsive');
});

// Open Modal Portfolio
$(document).on("click", ".openModalPortfolio", function (e) {
    e.preventDefault();

    let id = $(this).data('idportfolio');

    $("#form-content").html('');
    $("#modalVideo").removeClass('invisible');
    var url = `/portfolio/photos/${id}`;
    $.get(url,
        $(this)
            .addClass('modal-scrollfix')
            .find('#form-content')
            .html('Carregando...'),
        function (data) {
            // console.log(data);
            $("body").addClass('modal-open');
            $("#form-content").append(data);
        });
});

// Open Modal Videos
$(document).on("click", ".openModalVideo", function (e) {
    e.preventDefault();

    let id = $(this).data('idportfolio');

    $("#form-content").html('');
    $("#modalVideo").removeClass('invisible');
    var url = `/portfolio/video/${id}`;
    $.get(url,
        $(this)
            .addClass('modal-scrollfix')
            .find('#form-content')
            .html('Carregando...'),
        function (data) {
            // console.log(data);
            $("body").addClass('modal-open');
            html = `<iframe src="` + data +
                `?autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
            $("#form-content").addClass('iframe-responsive').append(html);
        });
});