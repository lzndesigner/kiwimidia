<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();

        User::create([
            'name' => 'Leonardo',
            'email' => 'contato@innsystem.com.br',
            'email_verified_at' => now(),
            'password' => Hash::make('@lzn94designer@'), // password
            'remember_token' => Str::random(10),
        ]);
        // Use Teste
        // User::factory(10)->create();
    }
}
