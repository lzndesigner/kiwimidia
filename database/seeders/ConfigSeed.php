<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('configs')->delete();

        Config::create([
            'meta_title' => 'Bem Vindo',
            'meta_description' => '',
            'meta_keywords' => '',
            'name_site' => 'Kiwimídia',
            'proprietary' => 'Murilo',
            'address' => '',
            'email_admin' => 'contato@kiwimidia.com',
            'telephone' => '(11) 93494-9993',
            'cellphone' => '',
            'hour_open' => '',
            'logo' => '/sem_imagem.jpg',

            'facebook' => 'kiwimidia',
            'twitter' => '',
            'instagram' => 'kiwimidia',
            'whatsapp' => '(11) 93494-9993',
        ]);
    }
}
