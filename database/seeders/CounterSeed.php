<?php

namespace Database\Seeders;

use App\Models\Counter;
use Illuminate\Database\Seeder;

class CounterSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('counters')->delete();

        Counter::create([
            'title_1' => 'Título 1',
            'number_1' => '0',
            'title_2' => 'Título 2',
            'number_2' => '0',
            'title_3' => 'Título 3',
            'number_3' => '0',
            'title_4' => 'Título 4',
            'number_4' => '0',
        ]);
    }
}
