<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkPixsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_pixes', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('description');
            $table->string('slug', 100)->unique();
            $table->index('slug'); // otimizar consulta
            $table->enum('status', array('habilitado', 'desabilitado'));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_pixes');
    }
}
