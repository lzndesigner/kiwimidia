<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'image',
        'imagens',
        'content',
        'slug',
        'tags',
        'featured',
        'videos',
        'status',
        'sort_order',
    ];

    public function images()
    {
        return $this->hasMany(PortfolioGallery::class);
    }
}
