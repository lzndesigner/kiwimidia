<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    use HasFactory;

    protected $fillable = [
        'title_1',
        'title_2',
        'title_3',
        'title_4',
        'number_1',
        'number_2',
        'number_3',
        'number_4',
        'time',
    ];

}
