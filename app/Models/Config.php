<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $fillable = [
        'meta_title',
        'meta_description',
        'meta_keywords',
        'name_site',
        'proprietary',
        'address',
        'cnpj',
        'email_admin',
        'telephone',
        'cellphone',
        'hour_open',
        'twitter',
        'facebook',
        'instagram',
        'whatsapp',
        'logo',
    ];
}
