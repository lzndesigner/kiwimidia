<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Config;
use App\Models\Page;
use App\Models\Service;
use App\Models\Portfolio;
use App\Models\Customer;
use App\Models\Team;
use App\Models\Counter;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $config_site;
    protected $getAbout;
    protected $getServices;
    protected $getPortfolios;
    protected $getCustomers;
    protected $counters;

    public function __construct()
    {
        $this->config_site = Config::get()->first();
        View::share('config_site', $this->config_site);
        
        $this->getAbout = Page::where('meta_title', 'Sobre')->first();
        View::share('getAbout', $this->getAbout);
        
        $this->getServices = Service::all();
        View::share('getServices', $this->getServices);
        
        $this->getPortfolios = Portfolio::orderBy(\DB::raw('-`sort_order`'), 'desc')->take(6)->get();
        View::share('getPortfolios', $this->getPortfolios);
        
        $this->getCustomers = Customer::orderBy(\DB::raw('-`sort_order`'), 'desc')->get();
        View::share('getCustomers', $this->getCustomers);
        
        $this->getTeams = Team::all();
        View::share('getTeams', $this->getTeams);
        
        $this->counters = Counter::get()->first();
        View::share('counters', $this->counters);
    }
}
