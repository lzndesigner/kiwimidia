<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Mail\SendMail;
use App\Models\Service;
use App\Models\LinkPix;
use App\Models\Portfolio;
use App\Models\Customer;
use App\Models\Team;


class HomepageController extends Controller
{
    /* Variables globais */
    public $folder = 'frontend.';

    public function index()
    {
        return view($this->folder . 'homepage ');
    }

    public function about()
    {
        $teams = new Team;
        $getTeams = $teams->get();

        return view($this->folder . 'about', compact('getTeams'));
    }

    public function services()
    {
        $services = new Service;
        $results = $services->get();

        return view($this->folder . 'services', compact('results'));
    }

    public function showService($slugService)
    {
        $service = new Service;
        $result = $service->where('slug', $slugService)->first();

        return view($this->folder . 'showService', compact('result'));
    }

    public function portfolio()
    {
        $portfolio = new Portfolio;
        $results = $portfolio::orderBy(\DB::raw('-`sort_order`'), 'desc')->get();

        return view($this->folder . 'portfolio', compact('results'));
    }

    public function portfolioVideos($id)
    {
        $portfolio = new Portfolio;
        $result = $portfolio->where('id', $id)->first();

        foreach(json_decode($result->videos) as $getVideos){
            $videos = str_replace('watch?v=', 'embed/', $getVideos);
        }
        
        return $videos;
    }

    public function portfolioPhotos($id)
    {
        $portfolio = new Portfolio;
        $result = $portfolio->where('id', $id)->first();
        $miniaturas = Portfolio::find($result->id)->images;
        
        return view($this->folder. 'showPortfolio', compact('result', 'miniaturas'));
    }

    public function showPortfolio($slugPortfolio)
    {
        $portfolio = new Portfolio;
        $result = $portfolio->where('slug', $slugPortfolio)->first();
        $miniaturas = Portfolio::find($result->id)->images;

        return view($this->folder . 'showPortfolio', compact('result', 'miniaturas'));
    }

    
    public function showLinkPix($slugLink)
    {
        $link_pix = new LinkPix;
        $result = $link_pix->where('slug', $slugLink)->first();

        return view($this->folder . 'showLinkPix', compact('result'));
    }

    public function clientes()
    {
        $clientes = new Customer;
        $results = $clientes->orderBy(\DB::raw('-`sort_order`'), 'desc')->get();

        return view($this->folder . 'clientes', compact('results'));
    }

    public function contato()
    {
        return view($this->folder . 'contato');
    }

    // Page - Contato
    public function sendMail(Request $request)
    {
        $rules = [
            'name'     => "required",
            'email' => 'required|email',
            'telephone' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'email.required' => 'email é obrigatório',
            'telephone.required' => 'assunto é obrigatório',
            'subject.required' => 'assunto é obrigatório',
            'message.required' => 'mensagem é obrigatório',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        try {
            Mail::send(new SendMail($request));
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('E-mail encaminhado com sucesso', 200);
    }
}
