<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Portfolio;
use App\Models\PortfolioGallery;

class PortfoliosController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.portfolios.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $portfolios = Portfolio::paginate($itensPage);

        return view($this->folder . 'index', [
            'portfolios' => $portfolios,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new Portfolio;
        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'image'               => "required",
            'content'             => "required",
            'slug'                => "required|unique:portfolios",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        if ($result['image']) {
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->tags = $result['tags'];
        if (isset($result['featured'])) {
            $model->featured = $result['featured'];
        } else {
            $model->featured = 0;
        }
        $model->status = $result['status'];
        $model->sort_order = $result['sort_order'];

        if (isset($result['videos'])) {
            $ar_videos = $result['videos'];
            $json_videos = json_encode($ar_videos);
            $model->videos = $json_videos;
        } else {
            $model->videos = '';
        }

        try {
            $model->save();

            $images = [];
            if ($result['imagens'][0]) {
                foreach ($result['imagens'] as $value) {
                    $newimages = explode(',', $value);
                    foreach ($newimages as $newimage) {
                        $format_url_newimage = str_replace(env('APP_URL'), '', $newimage);
                        $images[] = new PortfolioGallery([
                            'image' => $format_url_newimage
                        ]);
                    }
                }

                $model->images()->saveMany($images);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Portfólio salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugPortfolio)
    {
        $service = new Portfolio;
        $result = $service->where('slug', $slugPortfolio)->first();

        $miniaturas = Portfolio::find($result->id)->images;

        return view($this->folder . 'show', [
            'result' => $result,
            'miniaturas' => $miniaturas
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $portfolio = new Portfolio;
        $result = $portfolio->where('id', $id)->first();

        $miniaturas = Portfolio::find($id)->images;

        return view($this->folder . 'form', [
            'result' => $result,
            'miniaturas' => $miniaturas
        ]);
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new Portfolio;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'image'               => "nullable",
            'content'             => "required",
            'slug'                => "required|unique:portfolios,slug,$id,id",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        if ($result['image']) {
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->tags = $result['tags'];
        $model->featured = $result['featured'];
        $model->status = $result['status'];
        $model->sort_order = $result['sort_order'];

        if (isset($result['videos'])) {
            $ar_videos = $result['videos'];
            $json_videos = json_encode($ar_videos);
            $model->videos = $json_videos;
        } else {
            $model->videos = '';
        }

        try {
            $model->save();

            if ($result['imagens'][0] != null) {
                $images = [];

                foreach ($result['imagens'] as $value) {
                    $newimages = explode(',', $value);
                    foreach ($newimages as $newimage) {
                        $format_url_newimage = str_replace(env('APP_URL'), '', $newimage);
                        $images[] = new PortfolioGallery([
                            'image' => $format_url_newimage
                        ]);
                    }
                }

                $model->images()->saveMany($images);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Portfólio alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new Portfolio;

        try {
            // Delete Miniaturas do Portfolio
            $model_imagem = new PortfolioGallery;
            $findImages = $model_imagem->where('portfolio_id', $id);
            $findImages->delete();

            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY - IMAGE
     */
    public function destroyImage($portfolio_id, $image_id)
    {
        $model = new PortfolioGallery;

        try {
            $find = $model->where('portfolio_id', $portfolio_id)->where('id', $image_id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }

    public function change_position(Request $request, $id, $new_order)
    {
        $model = new Portfolio;
        $model = $model::find($id);

        $result = $request->all();

        $model->sort_order = $result['change_position'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
