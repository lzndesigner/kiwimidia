<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Service;

class ServicesController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.services.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $services = Service::paginate($itensPage);

        return view($this->folder . 'index', [
            'services' => $services,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new Service;
        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'image'               => "required",
            'content'             => "required",
            'slug'                => "required|unique:services",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        if($result['image']){
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Serviço salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugService)
    {
        $service = new Service;
        $result = $service->where('slug', $slugService)->first();

        return view($this->folder . 'show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $service = new Service;
        $result = $service->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new Service;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'image'               => "nullable",
            'content'             => "required",
            'slug'                => "required|unique:services,slug,$id,id",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        if($result['image']){
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Serviço alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new Service;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
