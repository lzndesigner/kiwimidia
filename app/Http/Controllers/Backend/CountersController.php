<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Counter;

class CountersController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.counters.'; // required ponto final

    public function index()
    {
        $counter = Counter::first();
        return view($this->folder . 'index', [
            'counter' => $counter
        ]);
    }

    public function update(Request $request, $id)
    {
        $model = new Counter;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'title_1'        => 'nullable',
            'number_1'       => 'nullable',
            'title_2'        => 'nullable',
            'number_2'       => 'nullable',
            'title_3'        => 'nullable',
            'number_3'       => 'nullable',
            'title_4'        => 'nullable',
            'number_4'       => 'nullable',
        ];

        $messages = [
            'title_1.required' => 'título #1 é obrigatório',
            'number_1.required' => 'número #1 é obrigatório',
            'title_2.required' => 'título #2 é obrigatório',
            'number_2.required' => 'número #2 é obrigatório',
            'title_3.required' => 'título #3 é obrigatório',
            'number_3.required' => 'número #3 é obrigatório',
            'title_4.required' => 'título #4 é obrigatório',
            'number_4.required' => 'número #4 é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->title_1 = $result['title_1'];
        $model->number_1 = $result['number_1'];
        $model->title_2 = $result['title_2'];
        $model->number_2 = $result['number_2'];
        $model->title_3 = $result['title_3'];
        $model->number_3 = $result['number_3'];
        $model->title_4 = $result['title_4'];
        $model->number_4 = $result['number_4'];
        $model->time = $result['time'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Contador alterado com sucesso', 200);
    }
}
