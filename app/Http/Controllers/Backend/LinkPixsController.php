<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\LinkPix;

class LinkPixsController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.link_pixs.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $link_pixs = LinkPix::orderBy('id', 'desc')->paginate($itensPage);

        return view($this->folder . 'index', [
            'link_pixs' => $link_pixs,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new LinkPix;

        $result = $request->all();

        $rules = [
            'name'          => "required",
            'slug'          => "required|unique:link_pixes",
            'description'               => "required",
            'status'              => "required",
        ];

        $messages = [
            'name.required'       => 'nome é obrigatório',
            'slug.required'       => 'url amigável é obrigatório',
            'slug.unique'       => 'url amigável já está sendo usada',
            'description.required'            => 'descriptionm é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->description = $result['description'];
        $model->status = $result['status'];
        $model->slug = $result['slug'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Link PIX salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugLinkPix)
    {
        $customer = new LinkPix;
        $result = $customer->where('slug', $slugLinkPix)->first();

        return view($this->folder . 'show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $customer = new LinkPix;
        $result = $customer->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = LinkPix::find($id);

        $result = $request->all();
        $rules = [
            'name'          => "required",
            'slug'          => "required|unique:link_pixes,slug,$id,id",
            'description'               => "required",
            'status'              => "required",
        ];

        $messages = [
            'name.required'       => 'nome é obrigatório',
            'slug.required'       => 'url amigável é obrigatório',
            'slug.unique'       => 'url amigável já está sendo usada',
            'description.required'            => 'descriptionm é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];


        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->description = $result['description'];
        $model->status = $result['status'];
        $model->slug = $result['slug'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Link PIX alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new LinkPix;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }


    public function change_position(Request $request, $id, $new_order)
    {
        $model = new LinkPix;
        $model = $model::find($id);

        $result = $request->all();

        $model->sort_order = $result['change_position'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
