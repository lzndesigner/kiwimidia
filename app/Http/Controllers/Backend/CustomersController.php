<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Customer;

class CustomersController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.customers.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $customers = Customer::orderBy(\DB::raw('-`sort_order`'), 'desc')->paginate($itensPage);

        return view($this->folder . 'index', [
            'customers' => $customers,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new Customer;
        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'web_link'          => "nullable|min:5",
            'image'               => "required",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'web_link.min'       => 'link minimo de 5 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->web_link = $result['web_link'];
        if($result['image']){
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->status = $result['status'];
        $model->sort_order = $result['sort_order'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Cliente salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugCustomer)
    {
        $customer = new Customer;
        $result = $customer->where('slug', $slugCustomer)->first();

        return view($this->folder . 'show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $customer = new Customer;
        $result = $customer->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new Customer;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'web_link'          => "nullable|min:5",
            'image'               => "nullable",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'web_link.min'       => 'link minimo de 5 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->web_link = $result['web_link'];
        if($result['image']){
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->status = $result['status'];
        $model->sort_order = $result['sort_order'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Cliente alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new Customer;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }


    public function change_position(Request $request, $id, $new_order)
    {
        $model = new Customer;
        $model = $model::find($id);

        $result = $request->all();

        $model->sort_order = $result['change_position'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
