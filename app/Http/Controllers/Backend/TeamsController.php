<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Team;

class TeamsController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.teams.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $teams = Team::paginate($itensPage);

        return view($this->folder . 'index', [
            'teams' => $teams,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new Team;
        $result = $request->all();

        $rules = [
            'name'          => "required",
            'description'    => "nullable|max:255",
            'image'               => "required",
            'facebook'             => "nullable",
            'instagram'             => "nullable",
            'whatsapp'             => "nullable",
            'status'              => "required",
        ];

        $messages = [
            'name.required'       => 'Nome é obrigatório',
            'description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->description = $result['description'];
        if ($result['image']) {
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->facebook = $result['facebook'];
        $model->instagram = $result['instagram'];
        $model->whatsapp = $result['whatsapp'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Equipe salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugTeam)
    {
        $team = new Team;
        $result = $team->where('slug', $slugTeam)->first();

        return view($this->folder . 'show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $team = new Team;
        $result = $team->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new Team;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'name'          => "required",
            'description'    => "nullable|max:255",
            'image'               => "nullable",
            'facebook'             => "nullable",
            'instagram'             => "nullable",
            'whatsapp'             => "nullable",
            'status'              => "required",
        ];

        $messages = [
            'name.required'       => 'Nome é obrigatório',
            'description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'image.required'            => 'imagem é obrigatório',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->description = $result['description'];
        if ($result['image']) {
            $format_url_image = str_replace(env('APP_URL'), '', $result['image']);
            $model->image = $format_url_image;
        }
        $model->facebook = $result['facebook'];
        $model->instagram = $result['instagram'];
        $model->whatsapp = $result['whatsapp'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Equipe alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new Team;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
